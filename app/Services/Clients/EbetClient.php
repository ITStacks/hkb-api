<?php

namespace App\Services\Clients;

use GuzzleHttp\Client;
use phpseclib3\Crypt\RSA;
use phpseclib3\Crypt\RSA\PrivateKey;
use phpseclib3\Crypt\PublicKeyLoader;

class EbetClient
{
    private Client $client;

    public const END_POINT = 'http://hkbid2.ebet.im:8888/api';
    public const CHANNEL_ID = 3063;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function createUser(array $data): array
    {
        /** @var ResponseInterface */
        $response = $this->client->request('POST', self::END_POINT . '/syncuser', [
            'json' => [
                "channelId" => self::CHANNEL_ID,
                "username" => $data['username'],
                "subChannelId" => 0,
                "signature" => $this->getSignature($data['username']),
                "currency" => "IDR",
                "china" => 1
            ]
        ]);

        return $response->getBody()->getContents();
    }

    public function getGameLink(): array
    {
        $timestamp = now()->unix();
        $response = $this->client->request('POST', self::END_POINT . '/launchUrl', [
            'json' => [
                "channelId" => self::CHANNEL_ID,
                "timestamp" => $timestamp,
                "signature" => $this->getSignature(self::CHANNEL_ID . $timestamp),
                "currency" => "IDR",
            ]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    private function getSignature($text): string
    {
        /** @var PrivateKey */
        $key = PublicKeyLoader::loadPrivateKey(file_get_contents(resource_path() . '/keys/ebet_private.key'))
            ->withPadding(RSA::SIGNATURE_PKCS1)
            ->withHash('md5');
        return base64_encode($key->sign($text));
    }
}
