<?php

namespace App\Services\Clients;

interface ClientInterface
{
    public function initiateTransfer(): void;
    public function getGameLink(): void;
}
