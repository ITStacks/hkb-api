<?php

namespace App\Models;

use App\Scopes\IsArchivedScope;
use App\Traits\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Model;
use Auth, DB;

class User_coin extends Model
{
    use HasCompositePrimaryKey; // *** THIS!!! ***

    protected $table      = 'user_coin';
    protected $primaryKey =  ['user_id', 'vendor_id'];
    public $incrementing = false;
    protected $guarded = [];

    public $timestamps = FALSE;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new IsArchivedScope);
    }

    protected $fillable = [
        'web_id',
        'vendor_id',
        'user_id',
        'coin',
        'val_key'
    ];


    public static function getBalance($user_id, $vendor_id, $withoutGlobalScopes = 0)
    {
        $user_coin = User_coin::onWriteConnection()->where([
            ['user_id', '=', $user_id],
            ['vendor_id', '=', $vendor_id]
        ]);

        if ($withoutGlobalScopes) {
            $user_coin->withoutGlobalScopes();
        }

        $user_coin = $user_coin->first(['coin', 'val_key']);
        if(empty($user_coin)){
            return 0;
        } 
        
        $user_coin = $user_coin->toArray();
        $bal = $user_coin['coin'];
        $val_key = $user_coin['val_key'];

        if ($vendor_id == 0) {
            $hash =  hash('sha256', $user_id . $vendor_id . $bal);
            if ($val_key != $hash) {
                return -1;  //error
            }
        }
        return $bal;
    }

    public static function updateBalance($user_id, $vendor_id, $type, $amount)
    { 

        if ($type == 'DB') {
            $current_balance = DB::raw('COALESCE(coin,0)-' . $amount);
        } else if ($type == 'CR') {
            $current_balance = DB::raw('COALESCE(coin,0)+' . $amount);
        }

        $update = User_coin::updateOrCreate(
            [
                "web_id"   => config('global.web_id'),
                'user_id'  => $user_id,
                'vendor_id' => $vendor_id
            ],
            [
                'coin' => $current_balance,
                'val_key' => DB::raw('SHA2(CONCAT(`user_id`,vendor_id, `coin`), 256)')
            ]
        );
    }

    public static function mainIncrement($value)
    {
        static::userAuth()->main()->increment('coin', $value);
    }

    public static function mainDecrement($value)
    {
        static::userAuth()->main()->decrement('coin', $value);
    }

    public static function vendorIncrement($value, $vid)
    {
        static::userAuth()->vendor($vid)->increment('coin', $value);
    }

    public static function vendorDecrement($value, $vid)
    {
        static::userAuth()->vendor($vid)->decrement('coin', $value);
    }

    public function scopeUserAuth($query)
    {
        return $query->where('user_id', Auth::user()->user_id);
    }

    public function scopeMain($query)
    {
        return $query->where('vendor_id', 0);
    }

    public function scopeVendor($query, $vid)
    {
        return $query->where('vendor_id', $vid);
    }
}
