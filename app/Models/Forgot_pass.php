<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Forgot_pass extends Model
{
    protected $table      = 'forgot_pass';
    protected $primaryKey = 'id';

    protected $fillable = ['web_id', 'user_id', 'type', 'isdone'];

    public $timestamps = FALSE;
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    
}
