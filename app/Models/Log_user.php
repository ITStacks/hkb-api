<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Log_user extends Model
{
    protected $table      = 'log_user';
    protected $primaryKey = '';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public static function new_log($user_name, $log_id, $desc){
        $channel = get_channel_id();
    	static::insert([
            "web_id"        => config('global.web_id'),
    		'user_name'     => $user_name,
    		'log_id'        => $log_id,
    		'log_desc'      => $desc,
    		'log_date'      => date('Y:m:d H:i:s'),
    		'log_ip'        => getIP(),    
    		'channel'       => $channel   
    	]);
    }

}
