<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Complaints_memo extends Model
{
    protected $table = "complaints_memo";
    protected $primaryKey = 'id';
    const UPDATED_AT = null;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
}
