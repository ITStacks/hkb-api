<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Settings_image extends Model
{
    protected $table      = 'settings_image';
    protected $primaryKey = 'settings_image_id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public function scopePopupImg($query){
        return $query->where("image_folder", "popup");
    }
    public function scopePopupLoginImg($query){
        return $query->where("image_folder", "popuplogin");
    }
}
