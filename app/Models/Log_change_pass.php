<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Log_change_pass extends Model
{
    protected $table      = 'log_change_pass';
    protected $primaryKey = 'id';

    protected $fillable = ['web_id', 'user_id', 'old_password', 'created_date'];

    public $timestamps = FALSE;
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    
}
