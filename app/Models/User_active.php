<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Auth;

class User_active extends Model
{
    protected $table      = 'user_active';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public static function add_active_user($user){

        $user_id = $user->user_id;
        $session = session()->getId();

        User::activate_user($user_id);

        User::withoutGlobalScopes()->where("user_id",  $user_id)
          ->update([
                  "last_session"  =>  $session
          ]);

        $check_user_active = User_active::where("user_id", $user_id)->count("user_id");
        if($check_user_active > 0){
          User_active::where("user_id", $user_id)->update([
                        "sess_id"   => $session, 
                        "channel"   => get_channel_id(),
                        "ip"        => getIP(),
                        "address"   => request()->getHost()
                      ]);
          // return $this->loginFailed($user, trans('auth.login_multiple'), $request);
        }else{
            //Add user active list  

        	$channel = get_channel_id();
            $referral=User::where('user_id',$user->user_id)->first(['ref_id','curr_id']);
            $referralName ="";

            if(!empty($referral->ref_id)){
                $referral_info=User::where('user_id',$referral->ref_id)->first(['user_name']);
                if(!empty($referral_info))$referralName=$referral_info->user_name;
            }

        	static::insert([
                "web_id"        => config('global.web_id'),
                "user_id"       => $user->user_id,
        		"user_name"     => $user->user_name,
                "ref_name"      => $referralName,
                "curr_code"     => "IDR",
                "status"        => $user->status,
        		"time_enter"    => date("Y-m-d H:i:s"),
        		"time_update"   => date("Y-m-d H:i:s"),
        		"sess_id"       => $session,
        		"channel"       => $channel,
                "ip"            => getIP(),
                "address"       => app('request')->getHost()
        	]);
        }

        return $session;
    }
 
}
