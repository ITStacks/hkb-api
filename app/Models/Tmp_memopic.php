<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Tmp_memopic extends Model
{
    protected $table      = 'tmp_memopic';
    protected $primaryKey = 'memopic_id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public function scopeUserAuth($query){
        return $query->where('user_id', Auth::user()->user_id);
    }

   
}
