<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Wd_block extends Model
{
    protected $table = 'wd_block';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public function scopeUserAuth($query){
    	return $query->where('user_id', Auth::user()->user_id);
    }
}
