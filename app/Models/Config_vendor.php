<?php

namespace App\Models;
 
use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Config_vendor extends Model
{
    protected $table      = 'config_vendor';
    protected $primaryKey = 'vendor_id';

    public $timestamps = FALSE;
     
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    
}
