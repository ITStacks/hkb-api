<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config_curr extends Model
{
    protected $table      = 'config_curr';
    protected $primaryKey = 'curr_id';

    public $timestamps = FALSE;
    
    public function user(){
        return $this->hasMany('App\Models\User', 'curr_id', 'curr_id');
    }
}
