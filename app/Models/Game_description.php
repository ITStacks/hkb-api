<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Game_description extends Model
{
    protected $table      = 'game_description';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
}
