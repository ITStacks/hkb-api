<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\User_coin;
use Auth;

class User extends Authenticatable
{
	use Notifiable;

    protected $table      = 'user';
    protected $primaryKey = 'user_id';

    public $timestamps = FALSE;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'user_name', 'user_email', 'user_pass','web_id','user_descr','user_phone','accgroup_id','user_bank','user_acc_name','user_acc_number','curr_id','joindate','last_session','status','ref_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_pass', 'user_pin',
    ];

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope); 

        // static::created(function($user){
        //     $array=array(
        //             'user_id' => $user->user_id ,
        //             'web_id' => $user->web_id ,
        //             'currency' => $user->curr_id ,
        //             'vendor_id' => 0 ,
        //       );
        //     $user->user_coin()->create($array); 
        // }); 
    }

    public function getAuthPassword()
    {
        return $this->user_pass;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken(){
        return null;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value){
        return null;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName(){
        return null;
    }

    public function user_coin(){
        return $this->belongsTo('App\Models\UserCoin','user_id');
    }
    
    public function currency(){
        $config_curr = $this->belongsTo('App\Models\DbConfig\Config_curr', 'curr_id', 'curr_id')->select(['code']);
        return $config_curr->value('code');
    }

    public function currencyName(){
        $config_curr = $this->belongsTo('App\Models\DbConfig\Config_curr', 'curr_id', 'curr_id')->select(['name']);
        return $config_curr->value('name');
    }

    public function currencyCode(){
        $config_curr = $this->belongsTo('App\Models\DbConfig\Config_curr', 'curr_id', 'curr_id')->select(['code']);
        return $config_curr->value('code');
    }

    public function bankName(){
        $config_bank = $this->belongsTo('App\Models\DbConfig\Config_bank', 'user_bank', 'bank_id')->select(['bank_name']);
        return $config_bank->value('bank_name');
    }

    public function mainBalance(){
        return User_coin::getBalance(Auth::user()->user_id,0);
    }

    public function vendorBalance($vendor_id){
        return User_coin::getBalance(Auth::user()->user_id,$vendor_id);
    }

    public function referralName()
    {
        $user = $this->hasOne('App\Models\User', 'user_id', 'ref_id')->select(['user_name']);
        return $user->value('user_name');
    }
    
    public function scopeActive($query){
        $query->where('status', 1);
    } 

    public function scopeNotDeleted($query){
        $query->where('isdeleted', 0);
    } 
    
    public static function activate_user($user_id){

        User::withoutGlobalScopes()->where("user_id",  $user_id)
          ->update([ 
                  "isarchived"   =>  0
          ]);

        User_coin::onWriteConnection()->withoutGlobalScopes()->where("user_id",  $user_id)
          ->update([
                "isarchived"   =>  0
          ]);  

    }
}
