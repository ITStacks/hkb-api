<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;

class Adm_transaction_day extends Model
{
    //
    protected $table = 'adm_transaction_day';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    // protected static function boot(){
    //     parent::boot();

    //     if(Gate::denies('isInternalUser'))
    //         static::addGlobalScope(new PartnerScope);
    // } 

}
