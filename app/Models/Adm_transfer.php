<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Auth,Exception;

class Adm_transfer extends Model
{
    protected $table      = 'adm_transfer';
    protected $primaryKey = 'transfer_id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }

    public static function insertTryCatch($array_insert){
        try 
        {
            $trans_id=Adm_transfer::insertGetId( $array_insert, 'trans_id'); 
            return $trans_id;
        }
        catch(\Exception $exception){
            return 0;        
        }
        
    }

    public function scopeUserAuth($query){
    	return $query->where('user_id', Auth::user()->user_id);
    }

    public function scopeDone($query){
    	return $query->where('status', 1);
    }

    public function scopeNotDone($query){
    	return $query->where('status', 0);
    }
    
}
