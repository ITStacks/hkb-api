<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Join_trans_day extends Model
{
    protected $table      = 'join_trans_day';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }

}
