<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;

class Join_lastorder extends Model
{
    //
    protected $table = 'join_lastorder';
    protected $primaryKey = 'join_id';

    public $timestamps = FALSE;
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    // protected static function boot(){
    //     parent::boot();

    //     if(Gate::denies('isInternalUser'))
    //         static::addGlobalScope(new PartnerScope);
    // }
}
