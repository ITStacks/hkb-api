<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Config_bank extends Model
{
    protected $table      = 'config_bank';
    protected $primaryKey = 'bank_id';

    public $timestamps = FALSE;
       
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public  function scopeDisplay($query){
    	return $query->visible()->notDeactive()->orderBy('bank_order');
    }

    public  function scopeActive($query){
        return $query->where('status', 1);
    }

    public  function scopeVisible($query){
        return $query->where('isvisible', 1);
    }

    public  function scopeNotDeactive($query){
    	return $query->where('status', '<>', 4);
    }

}
