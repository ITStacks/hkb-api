<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Template_page extends Model
{
    protected $connection = 'db_config';
    protected $table      = 'template_page';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

}
