<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

use App\Models\Config_menu;

class Menu extends Model
{ 
    protected $table      = "menu";
    protected $primaryKey = "id";
     
    public $timestamps = FALSE;
       
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    
    public static function getAllMenuLink(){

        $array=array();
        
    	$menus=Menu::join('config_menu as cm','menu.menu_id','=','cm.menu_id')
    	 			->where('menu_ishidden',1)
    	 			->get(['cm.menu_link','cm.menu_name','cm.menu_id']);
    	if(!empty($menus)){
        	foreach($menus as $menu){
        		$array[$menu->menu_id][$menu->menu_link]=strtolower($menu->menu_name);//
        	}
        } 
        return $array;
    }
}

/*
{
  "1": {
    "registration": "register"
  },
  "2": {
    "mobile": "mobile"
  },
  "3": {
    "cardgames": "card games"
  },
  "4": {
    "dingdong": "dingdong"
  },
  "5": {
    "lottery": "togel"
  },
  "6": {
    "promotion": "promosi"
  },
  "7": {
    "referral": "referral"
  },
  "8": {
    "help": "bantuan"
  },
  "9": {
    "": "status bank"
  },
  "10": {
    "": "jackpot bar"
  },
  "11": {
    "": "footer banner"
  },
  "12": {
    "": "footer public"
  },
  "13": {
    "": "header banner"
  },
  "14": {
    "deposit": "deposit"
  },
  "15": {
    "withdraw": "withdraw"
  },
  "16": {
    "memo": "memo"
  },
  "17": {
    "user/promo": "info promo"
  },
  "18": {
    "profile/turnover": "info turnover"
  },
  "19": {
    "user/referral": "referral"
  }
}*/