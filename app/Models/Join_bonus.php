<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Join_bonus extends Model
{
    protected $table = 'join_bonus';
    // protected $primaryKey = '';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }

    public function scopeUserAuth($query){
        return $query->where('user_id', Auth::user()->user_id);
    } 

}
