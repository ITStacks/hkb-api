<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adm_config extends Model
{
    protected $table      = 'adm_config';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

}
