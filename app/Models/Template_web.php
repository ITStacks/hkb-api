<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Template_web extends Model
{
    protected $connection = 'db_config';
    protected $table      = 'template_web';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new WebIdScope);
    }


}
