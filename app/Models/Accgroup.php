<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Accgroup extends Model
{
    protected $table      = 'accgroup';
    protected $primaryKey = 'accgroup_id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public function scopeActive($query){
    	return $query->where('accgroup_isactive', 1);
    }
    
}
