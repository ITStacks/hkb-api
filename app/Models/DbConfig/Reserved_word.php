<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;

class Reserved_word extends Model
{
    protected $connection ='db_config';
    protected $table      = 'reserved_word';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;
}
