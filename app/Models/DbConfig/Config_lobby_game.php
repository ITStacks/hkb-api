<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;

class Config_lobby_game extends Model
{
    protected $connection ='db_config';
    protected $table = 'config_lobby_game';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;


    public static function getAllRNGGame(){

		$game_ = Config_game::all();
		//dd($game_);
		$config_game=array();

		foreach($game_ as $detil){
			$config_game[$detil->game_id]['vendor_id']=$detil->vendor_id;
			$config_game[$detil->game_id]['game_category']=$detil->game_category;
			$config_game[$detil->game_id]['game_type']=$detil->game_type;
			$config_game[$detil->game_id]['game_name']=$detil->game_name;
		}

		return $config_game;
    } 

    /** get config_game */
    public function config_vendor()
    {
        return $this->hasMany('App\Models\DbConfig\Config_vendor', 'vendor_id', 'vendor_id');
    }

    /** get vendor_id by Sportsbook
     *  @return collection
     */
    public static function getVendorIdBySportbooks()
    {
        return Config_lobby_game::select('vendor_id')->where([ ['game_type','SportsBook'], ['game_isactive', 1]])->distinct()->get()->pluck('vendor_id');
    }

    /** Get Config_Game_id by vendor_id & game type Sportbooks
     * @param int $vendor_id
     * @return Collection
     */
    public static function GetConfigGameIdBySportsbook(int $vendor_id)
    {

        return Config_lobby_game::select('config_game_id')->where([ ['vendor_id',$vendor_id],['game_type','Sportsbook'] ])
                ->distinct()
                ->get()
                ->pluck('config_game_id');
    }

    /** Detail Config_Game_id by $config_game_id & game type Sportbooks
     * @param int $config_game_id
     */
    public function detailConfigGameIdBySportsbook(int $config_game_id)
    {
        return Config_lobby_game::select('config_game_id','game_type')->where([ ['config_game_id', $config_game_id],['game_type','Sportsbook'] ])->first();
    }

}
