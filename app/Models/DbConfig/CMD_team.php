<?php

namespace App\Models\DbConfig;


use Illuminate\Database\Eloquent\Model;

class CMD_team extends Model
{
    protected $connection ='db_config';
    protected $table = 'cmd_team';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;


    /** get Team by Team id
     * @param array $team_id
     */
    public static function getTeamByTeamId(array $team_id)
    {
        $arry_team = [];
        foreach ($team_id as $index_team => $item_team_id)
        {
            $team_name = CMD_team::where('team_id', $item_team_id)
                ->get('team_name')->pluck('team_name');
            array_push($arry_team, $team_name);
        }

        return $arry_team;
    }
}
