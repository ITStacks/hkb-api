<?php

namespace App\Models\DbConfig;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Config_page_items extends Model
{
    protected $connection = 'db_config';
    protected $table 	  = "config_page_items";
    protected $primaryKey = "item_id";
 	
 	public $timestamps = FALSE;
       
    

}
