<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{
    protected $connection = 'db_config';
    protected $table      = 'vendors';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;
}
