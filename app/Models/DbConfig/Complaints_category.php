<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;

class Complaints_category extends Model
{
    protected $connection = 'db_config';
    protected $table = "complaints_category";
    protected $primaryKey = 'id';
    
}
