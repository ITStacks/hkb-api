<?php

namespace App\Models\DbConfig;
 
use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Config_vendor extends Model
{
    protected $connection ='db_config';
    protected $table      = 'config_vendor';
    protected $primaryKey = 'vendor_id';

    public $timestamps = FALSE;
     
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }

    /** get config_lobby_game */
    public function config_lobby_game()
    {
        return $this->belongsTo('App\Models\DbConfig\Config_lobby_game', 'vendor_id', 'vendor_id');
    }

    /** relation partner vendor */
    public function partnerVendor()
    {
        return $this->hasMany('App\Models\DbConfig\Partner_vendor', 'vendor_id', 'vendor_id');
    } 
}
