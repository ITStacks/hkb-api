<?php 
namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;

class Config_game extends Model
{
    protected $table = 'config_game';
    protected $primaryKey = 'game_id';
    protected $connection= 'db_config';

    public $timestamps = FALSE;

    public function scopeTogelDDCList($query){
    	return $query->where(["game_category"=>"TG", "vendor_id"=>3]);
    }

    /** Get Config_Game Win568*/
    public function win568Trans()
    {
        return $this->hasMany('App\Models\Trans\Sbowin568_cw_trans', 'game_id');
    }
}
