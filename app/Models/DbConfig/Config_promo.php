<?php

namespace App\Models\DbConfig;
 
use Illuminate\Database\Eloquent\Model;

class Config_promo extends Model
{
    protected $table      = 'config_promo';
    protected $primaryKey = 'promo_type_id';

    public $timestamps = FALSE;
     
    
}
