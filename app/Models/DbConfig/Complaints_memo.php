<?php

namespace App\Models\DbConfig;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Complaints_memo extends Model
{
    protected $connection = 'db_config';
    protected $table = "complaints_memo";
    protected $primaryKey = 'id';
    const UPDATED_AT = null;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
}
