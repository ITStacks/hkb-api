<?php

namespace App\Models\DbConfig;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Ext_prov_user extends Model
{
	protected $connection = 'db_config';
    protected $table      = 'ext_prov_user';
    protected $primaryKey = 'ext_user_id';

    public $timestamps = FALSE;
       
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
}