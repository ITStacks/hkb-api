<?php

namespace App\Models\DbConfig;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Config_livechat extends Model
{
	protected $connection ='db_config';
    protected $table      = 'config_livechat';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;
       
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }

}
