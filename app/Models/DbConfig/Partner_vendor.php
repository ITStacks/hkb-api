<?php 
namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\WebIdScope;

class Partner_vendor extends Model
{
    protected $connection ='db_config';
    protected $table      = 'partner_vendor';
    protected $primaryKey = 'vendor_id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new WebIdScope);
    }

    public static function get_active_vendor_id(){


    	$active_vendor=Partner_vendor::where([    	
                'web_id'     => config('global.web_id'),
                'status'     => 1,
    			])->get(['vendor_id','prefix']);

        return $active_vendor;

    }

    /** relation config vendor */
    public function config_vendor()
    {
        return $this->belongsTo('App\Models\DbConfig\Config_vendor', 'vendor_id', 'vendor_id');
    }


}
