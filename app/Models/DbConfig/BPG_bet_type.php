<?php

namespace App\Models\DbConfig;


use Illuminate\Database\Eloquent\Model;

class BPG_bet_type extends Model
{
    protected $connection ='db_config';
    protected $table = 'bpg_bet_type';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    /** Get all bet type with key and value pair
     */
    public static function getAllBetType()
    {
        return BPG_bet_type::get()->pluck('bet_name','bet_id');
    }
}
