<?php

namespace App\Models\DbConfig;


use Illuminate\Database\Eloquent\Model;

class CMD_bet_position_type extends Model
{
    protected $connection ='db_config';
    protected $table = 'cmd_bet_position_type';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    /** getBetPosition by bet_post & bet_type
     * @param string $bet_type
     * @param int $bet_post
     * return collection
     */
    public static function getBetPosition(String $bet_type, int $bet_post)
    {
        return CMD_bet_position_type::select('bet_type','bet_pos_description')->where([ ['bet_type', $bet_type],['bet_pos', $bet_post] ])->first();
    }
}
