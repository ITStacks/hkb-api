<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;

class IP_2Location extends Model
{
    protected $connection ='db_config';
    protected $table      = 'ip2location';
    protected $primaryKey = 'id';

    public static function getIPDetails($ip = null){
        $ip_details = static::whereRaw("INET_ATON('{$ip}') BETWEEN ip_start AND ip_end")
            ->latest("ip_start")
            ->first(["iso_code","country"]);

        if(empty($ip_details)){
            return null;
        }
        return $ip_details;
    }   
}
