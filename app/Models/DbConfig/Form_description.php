<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;

class Form_description extends Model
{
    protected $connection = 'db_config';
    protected $table = "form_description";
}
