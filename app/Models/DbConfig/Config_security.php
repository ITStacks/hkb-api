<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\WebIdScope;

class Config_security extends Model
{
    protected $connection ='db_config';
    protected $table      = 'config_security';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
}
