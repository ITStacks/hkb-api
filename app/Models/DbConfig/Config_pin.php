<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;

class Config_pin extends Model
{
    protected $connection ='db_config';
    protected $table      = 'config_pin';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

}
