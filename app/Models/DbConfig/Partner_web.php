<?php  
namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\WebIdScope;

class Partner_web extends Model
{
    protected $connection ='db_config';
    protected $table      = 'partner_web';
    protected $primaryKey = 'web_id';

    public $timestamps = FALSE;
 

    protected static function boot(){
        parent::boot();        
        //static::addGlobalScope(new WebIdScope);
    }

    
}
