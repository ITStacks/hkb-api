<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;
use App;

class Join_news extends Model
{
    protected $connection 	='db_config';
    protected $table      	= 'join_news';
    protected $primaryKey	= 'news_id';

    public $timestamps = FALSE;



}
