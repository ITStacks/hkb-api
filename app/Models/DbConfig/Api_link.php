<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;

class Api_link extends Model
{
    protected $connection ='db_config';
    protected $table      = 'api_link';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;
}
