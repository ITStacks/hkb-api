<?php 
namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;

class Config_game_category extends Model
{
    protected $table = 'config_game_category';
    protected $primaryKey = 'id';
    protected $connection= 'db_config';

    public $timestamps = FALSE;

}
