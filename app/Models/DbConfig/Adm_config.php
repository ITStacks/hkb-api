<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;

class Adm_config extends Model
{
    protected $connection ='db_config';
    protected $table      = 'adm_config';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;


    public static function getAdmConfigValue($adm_config_id){
		
		$value = Adm_config::firstWhere('id',$adm_config_id)->value;

		if(!is_null($value)){
			return $value;
		}

		return FALSE;
    }
}
