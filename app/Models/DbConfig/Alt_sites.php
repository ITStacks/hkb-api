<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\WebIdScope;

class Alt_sites extends Model
{
    protected $connection ='db_config';
    protected $table      = 'alt_sites';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
}
