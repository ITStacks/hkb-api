<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\I_config_lang;

class Config_lang extends Model implements I_config_lang
{
	protected $connection ='db_config';
    protected $table      = 'config_lang';
    protected $primaryKey = 'lang_id';

    public $timestamps = FALSE;

    public static function getLangId($locale){
    	return constant('static::'.$locale);
    }
    
}
