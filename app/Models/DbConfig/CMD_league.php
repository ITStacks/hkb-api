<?php

namespace App\Models\DbConfig;


use Illuminate\Database\Eloquent\Model;

class CMD_league extends Model
{
    protected $connection ='db_config';
    protected $table = 'cmd_league';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    /** getLeagueByid
     * @param int $league_id
     * return collection league_name
     */
    public static function getLeagueByid(int $league_id)
    {
        return CMD_league::select('league_name')->where('league_id', $league_id)->first();
    }
}
