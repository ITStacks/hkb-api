<?php

namespace App\Models\DbConfig;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\WebIdScope;

class Config_meta extends Model
{
    protected $connection ='db_config';
    protected $table      = 'config_meta';
    protected $primaryKey = 'id';

    protected $fillable = [
        'web_id', 'page', 'title', 'description', 'keyword', 'seo'
    ];
    
    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
}
