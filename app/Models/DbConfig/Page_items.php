<?php

namespace App\Models\DbConfig;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Page_items extends Model
{
    protected $connection = 'db_config';
    protected $table 	  = "page_items";
    protected $primaryKey = "id";
 	
 	public $timestamps = FALSE;
       
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
 	
}
