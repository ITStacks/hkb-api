<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

//USER NOTIFY
//1:MemoWelcome,2:ValidationAccept,3:ValidationReject,
//4:DepoAccept,5:DepoReject,6:WithdrawAccept,7:WithdrawReject,
//8:Memo,9:BankChanged
//,10:BonusDeposit,11:BonusReferral,12:BonusCommission
class User_notify extends Model
{
    protected $table      = 'user_notify';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    // public static function create_notify($user_id,$notify_id,$curr_id,$mes=""){

    //   User_notify::insert([
    //     'web_id'   => config("global.web_id"),//Auth::user()->operator_id,
    //     'user_id'        => $user_id,
    //     'notify_id'      => $notify_id,
    //     'curr_id'      => $curr_id ,
    //     'mes'      => $mes,
    //     'created_date'      => date('Y:m:d H:i:s'),
    //   ]);

    // }

}
