<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Adm_question extends Model
{
    protected $table      = 'adm_question';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    private function getLangID(){
        $lang_code = App::getLocale();
        $lang_id = Config_lang::getLangId($lang_code);
        return $lang_id;
    }

    public function scopeSecurityQuestion($query){
    	return $query->where("lang_id", $this->getLangID())->get(["question", "q_id"]);
    }

}
