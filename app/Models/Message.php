<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Message extends Model
{
    protected $table      = 'message';
    protected $primaryKey = 'message_id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public function scopeUserMessage($query, $userid){
    	return $query->from('message as a')
    		->leftJoin('operator as b', 'a.operator_id', '=', 'b.operator_id')
    		->where([
    			['a.message_id', '<>', ''],
    			['a.user_id', '=', $userid],
    			['a.deleted_operator_id', 'NOT LIKE', '%,0%'],
    			['a.operator_id', '<>', '1'],
    			['a.message_isread', '=', 0]
    		]);
    }

    public function scopeUserAuth($query){
        return $query->where('user_id', Auth::user()->user_id);
    }

    public function scopeNotMainOperator($query){
        return $query->where('operator_id', '<>', '1');
    }

    public function scopeUnread($query){
        return $query->where('message_isread', 0);
    } 

    public function scopeRead($query){
        return $query->where('message_isread', 1);
    }

    public function scopeNotDeleted($query){
        return $query->where(function($query2){
                $query2->where('deleted_operator_id', 'NOT LIKE', '%,0%')
                        ->orWhereNull('deleted_operator_id');
        });
    }


}

