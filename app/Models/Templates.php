<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Templates extends Model
{
    protected $connection = 'db_config';
    protected $table      = 'template';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    /** get Template By TemplateID
     * @param int $template_id
     */
    public static function getTemplateByTemplateID(int $template_id)
    {
        return Templates::where('id', $template_id)->first();
    }
}
