<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Trans_raw extends Model
{
    protected $table      = 'trans_raw';
    protected $primaryKey = 'trans_id';
    protected $guarded = ["trans_id"];

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public function TransPromo(){
        return $this->belongsTo('App\Models\Trans_promo', 'promo_code_id', 'promo_code_id');
    }
    
    public function scopeZeroStats($query){
        return $query->userAuth()->notApproved()->notRejected();
    }

    public function scopeZeroStatsAll($query){
        return $query->notApproved()->notRejected();
    }

    public function scopeUserAuth($query){
        return $query->where('user_id', Auth::user()->user_id);
    } 

    public function scopePromoCodeID($query, $id){
        return $query->where('promo_code_id', $id);
    }

    public function scopeApproved($query){
        return $query->where('trans_isapproved', 1);
    }

    public function scopeNotApproved($query){
        return $query->where('trans_isapproved', 0);
    }

    public function scopeRejected($query){
        return $query->where('trans_isrejected', 1);
    }

    public function scopeNotRejected($query){
    	return $query->where('trans_isrejected', 0);
    }

    public function scopeTypeDeposit($query){
        return $query->where('trans_act', 1);
    } 

    public function scopeTypeWithdraw($query){
        return $query->where('trans_act', 2);
    }

    public function scopeTypeTransferIn($query){
        return $query->where('trans_act', 3);
    }

    public function scopeTypeTransferOut($query){
        return $query->where('trans_act', 4);
    }

    public function scopeTypeCorrectionIn($query){
        return $query->where('trans_act', 5);
    }

    public function scopeTypeCorrectionOut($query){
        return $query->where('trans_act', 6);
    }

    public function scopeTypeBonusDeposit($query){
        return $query->where('trans_act', 10);
    }

    public function scopeTypeBonusReferral($query){
        return $query->where('trans_act', 11);
    }

    public function scopeTypeBonusCommission($query){
        return $query->where('trans_act', 12);
    }


    
}
