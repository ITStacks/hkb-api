<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Promo_code extends Model
{
    protected $table      = 'promo_code';
    // protected $primaryKey = '';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public function TransPromo(){
        return $this->hasMany('App\Models\Trans_promo', 'promo_code_id', 'promo_code_id');
    }

    public function TransRaw(){
        return $this->hasMany('App\Models\Trans_raw', 'promo_code_id', 'promo_code_id');
    }

    public function scopeUserAuth($query){
        return $query->where('user_id', Auth::user()->user_id);
    } 

    public function scopeActive($query){
        return $query->where('promo_isactive', 1);
    }

    public function scopeNotActive($query){
        return $query->where('promo_isactive', 0);
    }

    public function scopeDeleted($query){
        return $query->where('promo_isdeleted', 1);
    }

    public function scopeNotDeleted($query){
        return $query->where('promo_isdeleted', 0);
    }

    public function scopeVisible($query){
        return $query->where('promo_isvisible', 1);
    }

    public function scopeNotVisible($query){
        return $query->where('promo_isvisible', 0);
    }

    public function scopeStart($query){
        return $query->where('start_date', '<=', date('Y-m-d H:i:s'));
    }

    public function scopeNotExpired($query){
        return $query->where('exp_date', '>', date('Y-m-d H:i:s'));
    }

    public function scopeTypeReload($query){
        return $query->where('promo_type_id', 1);
    } 

    public function scopeTypeSpecial($query){
        return $query->where('promo_type_id', 2);
    }

    public function scopeTypeNotSpecial($query){
        return $query->where('promo_type_id', '<>', 2);
    }

    public function scopeTypeRegister($query){
        return $query->where('promo_type_id', 3);
    }

    public function scopeTypeNotRegister($query){
    	return $query->where('promo_type_id', '<>', 3);
    }

    public function scopeTypeActiveMember($query){
        return $query->where('promo_type_id', 4);
    } 

    public function scopeTypeCashback($query){
        return $query->where('promo_type_id', 5);
    } 

    public function scopeTypeFreeChip($query){
        return $query->where('promo_type_id', 6);
    } 

    public function scopeNeedDeposit($query){
        return $query->where(['req_dp'=>1, 'req_code'=>0])->orderByRaw('FIELD(promo_type_id,3,2,promo_type_id)');
    }

    public function scopeNeedCode($query){
        return $query->where(['req_dp'=>0, 'req_code'=>1]);
    }

    public function scopeFreeCoin($query){
        return $query->where(['req_dp'=>0, 'req_code'=>0]);
    }


}
