<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Acc extends Model
{ 
    protected $table      = 'acc';
    protected $primaryKey = 'acc_id';

    public $timestamps = FALSE;
    
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public function scopeActive($query){
        return $query->where('acc_isactive', 1);
    }

    public function scopeNotActive($query){
        return $query->where('acc_isactive', 0);
    }


    // public static function get_bank_name(){
    //     return $this->belongsTo('App\Models\Db1\Config_bank', 'bank_id', 'bank_id')->select(['bank_name']);
    // }

}
