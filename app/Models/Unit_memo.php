<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Unit_memo extends Model
{
    protected $table      = 'unit_memo';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
}
