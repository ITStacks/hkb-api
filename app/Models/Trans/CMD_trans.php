<?php

namespace App\Models\Trans;


use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CMD_trans extends Model
{
    protected $connection = 'wluser_trans_db';
    protected $table = 'cmd_trans';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new WebIdScope);
    }

    /** getTransactionByDate user,start_date,end_date
     * @param object $user
     * @param string $start_date
     * @param string $end_date
     */
    public static function getTransactionByDate(object $user, string $start_date, string $end_date)
    {
        return CMD_trans::whereDate('bet_time', '>=', $start_date)->whereDate('bet_time', '<=', $end_date)->where('user_name', $user->user_name)->orderBy('bet_time','desc')->get();
    }

    /**
     * get 10 transaction by date and game id
     * @param string $start_date
     * @param string @end_date
     * @param string $user_name
     * @param array $list_config_game_id
     * partition @param string @monthly
     * @param int $number_paginate
     */
    public static function getPaginateTransactionByDateAndGameID(string $start_date, string $end_date, string $user_name,array $list_config_game_id, string $monthly, int $number_paginate)
    {
        return CMD_trans::from(DB::raw('cmd_trans PARTITION (' . $monthly . ')'))
                    ->whereDate('bet_time', '>=', $start_date)
                    ->whereDate('bet_time', '<=', $end_date)
                    ->where('user_name', $user_name)
                    ->whereIn('game_id', $list_config_game_id)
                    ->orderby('trans_id', 'desc')
                    ->paginate($number_paginate);
    }
}
