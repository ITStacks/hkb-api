<?php

namespace App\Models\Trans;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Sbowin568_cw_trans extends Model
{
	protected $connection = 'wluser_trans_db';
    protected $table      = 'sbowin568_cw_trans';
    protected $primaryKey = 'id';

    public $timestamps = ['UPDATED_AT'];

    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new WebIdScope);
    }

    public function setUpdatedAtAttribute($value) {
        $this->attributes['updated_at'] = \Carbon\Carbon::now();
    }

    /** Get Config Game */
    public function config_game()
    {
        return $this->belongsTo('App\Models\DbConfig\Config_game', 'config_game_id');
    }

    /** getTransactionByDate user,start_date,end_date & config_Game_id
     * @param object $user
     * @param string $start_date
     * @param string $end_date
     * @param array config_game_id
     */
    public static function getTransactionByDate(object $user, array $config_game_id, string $start_date, string $end_date)
    {
        return Sbowin568_cw_trans::with('config_game')->whereDate('bet_time', '>=', $start_date)->whereDate('bet_time', '<=', $end_date)->where('user_name', $user->user_name)->whereIn('config_game_id',$config_game_id)->get();
    }

}
