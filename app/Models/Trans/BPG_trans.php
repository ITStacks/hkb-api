<?php

namespace App\Models\Trans;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class BPG_trans extends Model
{
    protected $connection = 'wluser_trans_db';
    protected $table = 'bpg_trans';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new WebIdScope);
    }
}
