<?php

namespace App\Models\Trans;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Sbobet_trans extends Model
{
    //
    protected $connection = 'wluser_trans_db';
    protected $table = 'sbobet_trans';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
	}

	public static function outstandingBet($user){
		$trans = Sbobet_trans::where('vendor_user_id', $user->user_id)->get();
		return $trans;
	}
	
 //    const id = 'id';
	// const subweb = 'subweb';
	// const trans_id = 'trans_id';
	// const last_update = 'last_update';
	// const version_key = 'version_key';
	// const currency = 'currency';
	// const user_name = 'user_name';
	// const match_id = 'match_id';
	// const game_type = 'game_type';
	// const trans_datetime = 'trans_datetime';
	// const winlost_datetime = 'winlost_datetime';
	// const odds = 'odds';
	// const bet_amount = 'bet_amount';
	// const bet_status = 'bet_status';
	// const winlost_amount = 'winlost_amount';
	// const league_id = 'league_id';
	// const home_id = 'home_id';
	// const away_id = 'away_id';
	// const match_datetime = 'match_datetime';
	// const bet_type = 'bet_type';
	// const choice_code = 'choice_code';
	// const parlay_ref_no = 'parlay_ref_no';
	// const bet_team = 'bet_team';
	// const odds_type = 'odds_type';
	// const sport_type = 'sport_type';
	// const home_hdp = 'home_hdp';
	// const away_hdp = 'away_hdp';
	// const islive = 'islive';
	// const home_score = 'home_score';
	// const away_score = 'away_score';
	// const after_amount = 'after_amount';
	// const isLucky = 'isLucky';
}
