<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Memo extends Model
{
    protected $table      = 'memo';
    protected $primaryKey = 'memo_id';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    public function scopeUserAuth($query){
        return $query->where('user_id', Auth::user()->user_id);
    }

    public function scopeNotMainOperator($query){
        return $query->where('operator_id', '<>', '1');
    }

    public function scopeUnread($query){
        return $query->where('memo_isread', 0);
    } 

    public function scopeRead($query){
        return $query->where('memo_isread', 1);
    }

    public function scopeNotDeleted($query){
        return $query->where(function($query2){
                $query2->where('deleted_operator_id', 'NOT LIKE', '%,0%')
                        ->orWhereNull('a.deleted_operator_id');
        });
    }

    public function scopePrev($query){
        return $query->where('memo_date', '>', date('Y-m-d H:i:s', time()-60));
    }
}
