<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Log_api extends Model
{
    protected $table      = 'log_api'; 

    public $incrementing = false;
    public $timestamps = FALSE;
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    
    public static function new_log($vendor_name, $direction, $method, $detail){
    	static::insert([
            "web_id"        => config('global.web_id'),
    		'vendor_name'	=> $vendor_name,
    		'direction'		=> $direction,
    		'method'		=> $method,
    		'detail'		=> $detail,
    		'created_date'	=> date('Y:m:d H:i:s'),
    		'ip'			=> getIP()
    	]);
    }
}
