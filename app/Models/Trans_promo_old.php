<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Trans_promo_old extends Model
{
    protected $table      = 'trans_promo_old';
    protected $primaryKey = 'trans_promo_id';

    public $timestamps = FALSE;

    public function PromoCode(){
        return $this->belongsTo('App\Models\Promo_code', 'promo_code_id', 'promo_code_id');
    }

    public function scopeUserAuth($query){
        return $query->where('user_id', Auth::user()->user_id);
    }

    public function scopePromoCodeID($query, $id){
        return $query->where('promo_code_id', $id);
    }

    public function scopeDone($query){
        return $query->where('trans_isdone', 1);
    }

    public function scopeNotDone($query){
        return $query->where('trans_isdone', 0);
    }

    public function scopeCanceled($query){
        return $query->where('trans_iscanceled', 1);
    }

    public function scopeNotCanceled($query){
        return $query->where('trans_iscanceled', 0);
    }
    
}
