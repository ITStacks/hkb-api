<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Adm_bank extends Model
{
    protected $table      = 'adm_bank';
    // protected $primaryKey = '';

    public $timestamps = FALSE;
    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }
    
}
