<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Promo_display extends Model
{
	protected $table      = 'promo_display';
	protected $primaryKey = 'promo_display_id';

	public $timestamps = FALSE;

	protected $fillable = ['promo_image_id','web_id','promo_lang','promo_text', 'promo_title'];
}