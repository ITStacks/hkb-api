<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;
use App;
use Auth;

class Join_news_lang extends Model
{
    protected $table      = 'join_news_lang';
    // protected $primaryKey = '';

    public $timestamps = FALSE;

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new WebIdScope);
    }

    private function getLangID(){
        $lang_code = App::getLocale();
        $lang_id = Config_lang::getLangId($lang_code);
        return $lang_id;
    }
    
    public function scopeNewsRunningText($query){
        $news_id=1;
        if(Auth::check()) $news_id=2;
    	return $query->where(['news_id'=> $news_id, 'lang_id'=>$this->getLangID()]);
    }

    public function scopeDepoRemark($query){
    	return $query->where(['news_id'=>5, 'lang_id'=>$this->getLangID()]);
    }

    public function scopeDepoBusyText($query){
    	return $query->where(['news_id'=>6, 'lang_id'=>$this->getLangID()]);
    }

    public function scopeWithdrawRemark($query){
    	return $query->where(['news_id'=>7, 'lang_id'=>$this->getLangID()]);
    }

    public function scopeWithdrawBusyText($query){
    	return $query->where(['news_id'=>8, 'lang_id'=>$this->getLangID()]);
    }
}

