<?php

namespace App\Models;

use App\Scopes\WebIdScope;
use Illuminate\Database\Eloquent\Model;

class Template_colour extends Model
{
    protected $connection = 'db_config';
    protected $table      = 'template_colour';
    protected $primaryKey = 'id';

    public $timestamps = FALSE;

    /**
     * Get the template web
     */
    public function templateWeb()
    {
        return $this->hasOne('App\Models\Template_web', 'colour_id');
    }
}
