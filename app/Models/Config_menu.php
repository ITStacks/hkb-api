<?php

namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;

class Config_menu extends Model
{ 
    protected $table      = "config_menu";
    protected $primaryKey = "menu_id";
     
    public $timestamps = FALSE;
        
    
}
