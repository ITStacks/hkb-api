<?php 
namespace App\Scopes;

use App\Models\DbConfig\Partner_web;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class WebIdScope implements Scope
{    //tampilkan partner id yg bisa diakses admin
	public function apply(Builder $builder, Model $model){ 

	 	$alias_name=explode($model->getTable()." as ",$builder->getQuery()->from);
	 	if(!isset($alias_name[1]))$alias_name=$model->getTable();
	 	else $alias_name=$alias_name[1];

	 	switch(strtolower($alias_name)){
	 		CASE "user":
	 		CASE "u":
	 		CASE "user_coin":
	 		CASE "uc":
				$builder->where($alias_name.".isarchived" , 0); break;
			default:break;

	 	}

    	$builder->where($alias_name.".web_id" , config('global.web_id'));
 

    }
}