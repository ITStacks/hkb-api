<?php 
namespace App\Http\Controllers;
use App\Repositories\ApiRepository; 
use App\Models\{Adm_transfer, Log_user,User_coin, User_active};
use App\Models\DbConfig\{Adm_config, Config_vendor};
use Auth, Session, Log, Config;
use App\Traits\UserTraits;
use Illuminate\Http\Request;

class ApiController extends Controller
{ 

    use UserTraits;

    public function getGameLink(Request $request){
        // for dekstop , mobile ,apk 
        $vendor_name    = $request->provider;
        $game_id        = $request->game_id; 

        $userLastSession = Auth::user()->last_session; 
        if($userLastSession != session('user_lastloginsession'))  {
            return response()->json(['status'=>300,'message'=>trans("auth.invalid_session")]);
        }

        $userStatus = $user=Auth::user()->status;        
        if($userStatus != 1){     
            return response()->json(['status'=>300,'message'=>trans("lang.user_status_4")]);
        }            

        $this->api = new ApiRepository;
        $link = $this->api->get_game_link($vendor_name,$game_id);   

        if(empty($link)){
            if($vendor_name == "PTIM"){
                return response()->json(['status'=>300 ,'message'=>trans('home.err_tryagain')]);        
            }
            
            return response()->json(['status'=>300,'message'=> trans('lang.game_maintenance')]);
        }

        if($request->ajax() || isWap()){
            return response()->json(['status'=>200,'link'=>$link ]); 
        }else{
            return response()->json(['status'=>300,'message'=> trans('lang.game_maintenance')]);
        }

    }

    public function initiateTransfer(Request $request) {
        $userLastSession = Auth::user()->last_session; 
        if($userLastSession != session('user_lastloginsession') || !(Auth::check()))  {
            return response()->json(['status'=>300,'message'=>trans("auth.invalid_session")]);
        }

        $vendor_name = $request->provider;
        $new = $request->new;
 
       // if($new == 1){
            $response = $this->transfer("ALL",$vendor_name);

            $transfer_status = $response;
            $transfer_amount = $min_transfer = 0; 

            // transfer: 0 = pending, 1 = success  ,< 0 = problem
            if(isset($response["status"])){
                $transfer_status = $response["status"];

                //min transfer alert -4
                if($response["status"] == -4){
                    $transfer_amount = $response["transfer_amount"];
                    $min_transfer = isset($response["min_transfer"])?$response["min_transfer"]:0;                    
                }
            } 

           switch ($transfer_status) {
                case 0:
                case -1:
                   return response()->json(['status'=>300 ,'message'=>trans('home.err_tryagain')]);
                   break;
                case -2:
                   return response()->json(['status'=>300 ,'message'=> trans('lang.error_open4')]);
                   break;
                case -3:
                    return response()->json(['status'=>300,'message'=> trans('lang.trf_error_balance')]);
                    break;
                case -4:
                    return response()->json(['status'=>300,'message'=> trans('lang.lobby_amount_min', ['amount' => balance_format($min_transfer)
                        ,'balance' => balance_format($transfer_amount)
                    ])]); 
                    break;
                case 1 :    
                    return response()->json(['status'=>200]); 
                default:  
                   return response()->json(['status'=>300,'message'=> trans('lang.error_open3')]);
                   break;

           }
       // }
        return response()->json(['status'=>200]); 
    }

    public function wapLobbyResponse($value='', $attr='', Request $request)
    {
        $text = backUrlHtml();
        switch ($value) {
            case '1': // show message game maintenance
                $text .= trans('lang.lobby_maintenance', ['destination'=>$attr]);
                break;
            case '2': // 2 and 3 normal text (status and Coming soon) messages
            case '3':
                $text .= $attr;
                break;
            case '4': // open lobby
                $request->provider = $attr;
                $request->new = 0;
                $request->game_id = $request->game;
                if(Auth::check()){
                    $this->api = new ApiRepository;
                    $balance = $this->api->getCurrentTotalBalance(Auth::user()->user_id); 
                    if($balance < 0){
                        $message = trans('lang.trf_error_balance');
                        return response(backUrlHtml().$message);
                    }
                        
                    if($balance > 0){
                        $request->new = 1;
                        $response_transfer = json_decode($this->initiateTransfer($request), TRUE);
                        if(isset($response_transfer['status']) && $response_transfer['status'] != 200){
                            return response(backUrlHtml().$response['message']);
                        }
                    }
                    
                    $request = $this->getGameLink($request);
                    $response = json_decode($request->getContent(), TRUE);
                    if(isset($response['status'])){
                        if($response['status'] == 200){
                            return redirect($response['link']);
                        }else{
                            return response(backUrlHtml().$response['message']);
                        }
                    }else{
                        return response(backUrlHtml().trans('lang.error_open3'));
                    }
                }
                break;
            case '5': // update balance
                if(Auth::check()){
                    $request = $this->updateBalance(Auth::user()->user_name);
                    $response = json_decode($request->getContent(), TRUE);
                    if(isset($response['status'])){
                        return response(backUrlHtml().$response['message']);
                    }
                }
                return response(backUrlHtml().trans('lang.error_open3'));

                break;
            default:
                abort(404);
                break;
        }
        return response($text);
    }

    public function updateBalance($user_name){ 
        $userLastSession = Auth::user()->last_session; 
        if($userLastSession != session('user_lastloginsession') || !(Auth::check()))  {
            return response()->json(['status'=>300,'message'=>trans("auth.invalid_session")]);
        }
        
        if(Session::has('firstLogin')){
            $force=1;
            $transfer=$this->transfer("ALL","MAIN",$force);             
        }     
        
        //if wap check balance , on js desktop , m already activate auto_yes function
        if(isWap()){
            $res = $this->check_balance("balance",0); 
        }

        $pending = $this->checkPendingTransfer();

        if($pending<0){
            return response()->json(['status'=>300,'message'=> trans('lang.trf_error_balance')]);
        }
        return response()->json( ['status'=>200, 'message'=> trans('home.success')]);
 
        // $force=1;
        // $transfer=$this->transfer("ALL","MAIN",$force); 
        
        // switch ($transfer) {
        //     case 1:
        //         return response()->json( ['status'=>200, 'message'=> trans('home.success')]);
        //         break;
        //     case 0:
        //        return response()->json(['status'=>300,'message'=> trans('lang.error_open1')]);
        //        break;
        //     case -1: 
        //     case -2:
        //         return response()->json(['status'=>300,'message'=> trans('home.err_tryagain')]);
        //         break;
        //     case -3:
        //        return response()->json(['status'=>300,'message'=> trans('lang.trf_error_balance')]);
        //        break;
        //     default:
        //        return response()->json(['status'=>300,'message'=> trans('lang.error_open3')]);
        //        break;
        // }
    }

    public function transfer($from,$vendor_name,$force=0){     
         
        $pending = $this->checkPendingTransfer();

        if($pending<0){
            return $pending;
        }

        $lock = $this->setUserCoinLock(Auth::user()->user_id);
        if(!$lock){
            return -1;
        }

        $balance = $this->check_main_balance( Auth::user()->user_id , Auth::user()->user_totaldpx); 
        if($balance < 0){
            return -3; // error bal
        }

        if($force==0){
            if($from!="ALL"){
                if($balance==0) {
                        $this->unsetUserCoinLock(Auth::user()->user_id);
                        return 1;
                }
            }
        }

        $user_id=Auth::user()->user_id;      

        $from ="ALL";
        $arr_from[]=array();
        $arr_from['type']=0;
        $arr_from['amount']=0;
        $arr_from['from']= $from;
        $arr_from['to']= '';

        $arr_to[]=array();
        $arr_to['type']=1;
        $arr_to['amount']=0;
        $arr_to['from']= '';
        $arr_to['to']= $vendor_name;
 
        $this->api = new ApiRepository;
        $vendors=$this->api->getActiveVendorList();  

        switch(strtoupper( $arr_from['from'])){  
            case "ALL":   
                $bal= array();

                $check_all_vendor=0;
                if(Session::has('firstLogin')){
                    $check_all_vendor=1; 
                     Session::pull('firstLogin'); 
                }

                if($check_all_vendor == 0){
                    //pull all to vendor_balance
                    $main_balance = $this->api->getCurrentTotalBalance(Auth::user()->user_id,1,$arr_to);  

                    // check min transfer 
                    $min_transfer = $vendors[$vendor_name]['min_transfer'] * $vendors[$vendor_name]['conversion'];
                    if($min_transfer>0){          
                        $check_vendor_balance = $this->api->getCurrentTotalBalance(Auth::user()->user_id);
                        if( ( floatval($check_vendor_balance) < $min_transfer) ){
                            $this->unsetUserCoinLock(Auth::user()->user_id);

                            $response= array(
                                    "status" =>  -4,
                                    "transfer_amount" => $main_balance,
                                    "min_transfer" => $min_transfer
                            );
                            return $response;
                        }
                    }


                    $arr_to["amount"] = $main_balance; 
                } else {    
                    #first_login & check all balance and transfer to destination provider                
                    foreach ($vendors as $key => $detail_vendor) { 
                        $bal[$key] = $this->api->check_balance($key);
                        if ($bal[$key] > 0) { 
                            $arr_from['vendor_name'] = $key; 
                            $this->api->transfer($key, $arr_from);
                        } 
                    }

                    $arr_to["amount"]=array_sum($bal);
                } 
                break;
            default:
                $this->api->transfer(strtoupper($arr_from['from']), $arr_from);
                break; 
        }    

        switch(strtoupper($arr_to['to'] )){  
            case "MAIN": 
                $status=1;
                break;
            default:
                $status=$this->api->transfer(strtoupper($arr_to['to']), $arr_to);
                break;

        }

        $this->unsetUserCoinLock(Auth::user()->user_id);

        $response= array(
                "status" =>  (int) $status,
                "transfer_amount" => $arr_to["amount"]
        );
        return $response;
    }

    public function getHomeAPI(){
        $this->api = new ApiRepository;
        $vendor = $this->api->getActiveVendorList();

        $data['message'] = 'Coming Soon'; // delete this variable if no longer used

        if(isset($vendor[config('global.code_hkb_gaming')])){
            $data['HKB_Jackpot'] = $this->api->get_hkbg_jackpot();        
        }else if(isset($vendor[config('global.code_hkb')])){
            $data['HKB_Jackpot'] = $this->api->get_hkb_jackpot();
        }
        return response()->json($data);
    }

    public function resultDetail(Request $request)
    {
        $this->api = new ApiRepository;
        return $this->api->get_number_details($request->game_id, $request->room_id, $request->period);
    }

    public function aginLobby(Request $request){

        /**
         * sample request
         * http://dev-user-o.focu588.com/aginLobby?id=TPK_2946038&type=12&stamp=1607595498178&feature=d99d9bdbad6d76d5a18643a076cb23eb
         * Constant
          *  Value =6            deposit
          *  Value =9            register real account
          *  Value =12           exit game
          *  Value =13           customer service page
        */
        if(trim($request->get('type'))==6){
            return redirect()->route('deposit');
        }
        return redirect()->route('home');
    }
    
    public function hkb_auth_sess(Request $request){

            if(!$this->validate_ip()){
                abort(404);
            }

            $name=$currency=$aff="";
            $status="FAILED";
            $parameter = $_SERVER['QUERY_STRING'];

            $params   = (json_encode($parameter));
            $method   = 'hkb-auth';
            $log0     = 0;
            $log1     = 0;

            $key=$request->query('hash') ?? '';
            $var=$request->query('token') ?? '';
            $username = $request->query('username') ?? '';
            $opid = $request->query('operatorid') ?? '';

            if( $key=='' ||  $var=='' || $username=='' || $opid==''){
                echo json_encode(['code' => 503]);
                exit;
            }
        
            $transfer_balance=$request->query('transfer_balance');

            $var_unit = explode("_", $username);
            $prefix= $var_unit[0];
            $user_name_no_prefix= $var_unit[1];
            $aff=$prefix."_";
    
            $vendor=Config_vendor::withoutGlobalScopes()
                                ->join('partner_web as pw', 'pw.web_id', '=', 'config_vendor.web_id')
                                ->where('vendor_operator_id',$opid)->first(['pw.web_id','vendor_id','vendor_name',  'vendor_prefix', 'vendor_operator_id', 'vendor_key','maint_api', 'pw.db_name']); 
            if(empty($vendor)){
                $message='';
                $error=400;              
                $vendor_name=$web_id=0;
            }else{
                $vendor_id=$vendor->vendor_id;
                $web_id= $vendor->web_id;
                $this->api= new ApiRepository($web_id);
                $vendor_operator_id= $vendor->vendor_operator_id; 
                $vendor_key= $vendor->vendor_key; 
                $vendor_name= $vendor->vendor_name; 
                $vendor_prefix= $vendor->vendor_prefix;  
                $db_name= $vendor->db_name;
                if($log0 == 1){ 
                    $this->api->record_api_call($vendor_name, $log0, $method, $params, $web_id);
                }  
                
                $secretkey=hash('sha256',$vendor_operator_id.$username.$var.$vendor_key); 
                $error=-1;
                
                if(!isset($key)||!isset($var)) $error=500;
                while($error<0){
                   if($key != $secretkey){ $error=500; break;} 

                    $r=User_active::on($db_name)->withoutGlobalScopes()->where([
                        ['sess_id','=',$var],
                        ['web_id','=',$web_id],
                        ['user_name','=',$user_name_no_prefix]
                        ])->first(['user_name','user_id','ref_name', 'curr_code', 'status']); 
                    if(empty($r)){
                        $message=''; 
                        $error=400; break;
                    }
                        
                        $status=$r->status; 
                        if($status!=1){
                            $message=''; 
                            $error=400; break;
                        }
                        
                        $user_name=$vendor_prefix.$r->user_name;
                        $user_id=$r->user_id; 
                        $channel=$r->channel;
                        $time_update=$r->time_update;
                        $ref_name=($r->ref_name) ? $vendor_prefix.$r->ref_name : '';
                        $currency=$r->curr_code;

                        $error=0;

                        if($transfer_balance==1){     

                            Session::put('selected_database',$db_name);
                            Config::set('database.default', $db_name);

                            $force=1;
                            $user_detail['web_id']=$web_id;
                            $user_detail['user_name']=$r->user_name;
                            $user_detail['user_id']=$user_id;
                             
                            $transfer=$this->transfer_api($user_detail,"ALL","HKB",$force);   
                            $this->api->record_api_call($vendor_name, $log0, "api/auto-depo", "res=$transfer-".$params, $web_id);     
            
                        }
 
                        try {             
                            User_active::on($db_name)->withoutGlobalScopes()->where('user_id', $user_id)->update(['time_update'=>date("Y-m-d H:i:s")]);  
                        }catch (\Exception $e) {
                            sleep(3);
                            User_active::on($db_name)->withoutGlobalScopes()->where('user_id', $user_id)->update(['time_update'=>date("Y-m-d H:i:s")]);
                        } 

                        $status="OK"; break;
                }
            } 
                    if($error==0){
                        $arrayRes=array( 
                                            "username"      =>  strtoupper($user_name), 
                                            "referral"      =>  strtoupper($ref_name),
                                            "currency"  =>  strtoupper($currency),
                                            "op"        =>  ($opid),  
                                            "code"      =>  0,  
                                            );
                    }else{ 
                        $arrayRes=array(
                                            "code"      =>  $error);
                    }


            $params   = (json_encode($arrayRes));
            $method   = 'res hkb-auth';
            
            if($log1 == 1){ 
                $this->api->record_api_call($vendor_name, $log1, $method, $params, $web_id);
            }
                

                    echo json_encode($arrayRes);
                        exit;
    }
    

    public function auth_sess($vendor_name, Request $request){

            if(!$this->validate_ip(55)){
                abort(404);
            }
            $vendor_name = strtoupper(trim($vendor_name));
            switch ($vendor_name) {
                case 'VIVO':
                    $vendor_name = "VG";
                    $params = $request->all();

                    if( ($user = $this->user_session($params['token'])) !== FALSE ){
                        
                        if(isset($user->web_id)){
                            config(['global.web_id' => $user->web_id]);
                        }                     
                        
                        $this->api = new ApiRepository;
                        return $this->api->authenticate_session($vendor_name,$user,$params);
                    }

                    break;
                
                default:
                    # code...
                    break;
            }
            

    }

    private function user_session($session=null){
        if( !empty($session) ){
            $user = User_active::withoutGlobalScopes()
				->where('sess_id', $session)
				->first();
            // return (empty($user)) ? false : $user;
            return $user;
        }
        return false;
    }

    private function validate_ip($id=53){ //53 = HKB
        $ip = getIP();
        $check_ip = Adm_config::where('id', $id)->first();
        if(empty($check_ip)){
            return false;
        }else{
            $ips = explode(',', $check_ip->value);
            $true = 0;
            foreach ($ips as $value) {
                if ( strpos( $value, '/' ) == false ) {
                    if($ip == $value){
                        $true++;
                    }
                }else{
                    $ip_in_range = ip_in_range($ip, $value);
                    if($ip_in_range){
                        $true++;
                    }
                }
            }
            if(!$true){
                return false;
            }
        }
        return true;
    }


    
    public function transfer_api($user_detail,$from,$vendor_name,$force=0){       

            $web_id=$user_detail['web_id'];
            $this->api= new ApiRepository($web_id);

            $user_id=$user_detail['user_id']; 

            $pending=self::checkPendingTransferApi($user_id,1); 

            if($pending<0)return $pending;
            
            $lock = $this->setUserCoinLock($user_id);
            if(!$lock)
                return -1;

            $bal= $this->api->getCurrentUserBalance($user_id);
             // json1($bal);
            if($bal==0) {
                $this->unsetUserCoinLock($user_id);
                return 0; 
            }

            //update balance jadi 0

            $from =$from;
            $arr_from[]=array();
            $arr_from['type']=0;
            $arr_from['amount']=0;
            $arr_from['from']= $from;
            $arr_from['to']= '';
            $arr_from['user_id']= $user_id;

            $arr_to[]=array();
            $arr_to['type']=1;
            $arr_to['amount']=0;
            $arr_to['from']= '';
            $arr_to['to']= $vendor_name; 
            $arr_to['user_id']= $user_id;

            switch(strtoupper( $arr_from['from'])){ 
                case "HKBG":    
                   $this->api->transfer('HKBG',$arr_from);
                    break;
                case "DDC":     
                   $this->api->transfer('DDC',$arr_from);
                    break;
                case "HKB":    
                    $this->api->transfer('HKB',$arr_from);
                    break;
                case "ALL":   
                    break; 
            }     

            switch(strtoupper($arr_to['to'] )){  
                case "HKB":    
                    $status=$this->api->transfer('HKB',$arr_to);
                    break;
                case "MAIN":    $status=1;
                    break;

            }
            $this->unsetUserCoinLock($user_id);
            return $status; 
        }

    public function checkPendingTransferApi($user_id,$check_only=0){ 
 
        $row= Adm_transfer::where([
            ['user_id','=', $user_id],
            ['status','=',0]
                ])->first(['status','date','transfer_id','vendor_id','type','amount','user_id']);
        if(!empty($row)){
            if($row->status==0){ 
                return -1; 
            } 
        }
        return 0;

    } 
}

