<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Clients\EbetClient;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * @TODO make client dyanamic using client providers pattern
     */
    public function createUser(EbetClient $client, Request $request)
    {
        return $client->createUser([]);
    }

    public function gameLink(EbetClient $client)
    {
        return $client->getGameLink();
    }
}
