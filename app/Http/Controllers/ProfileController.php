<?php

namespace App\Http\Controllers;

use App\Repositories\{ApiRepository, CustomRepository};
use App\Models\{Join_trans_day , User, Memo, Log_change_pass};
use App\Models\DbConfig\{Config_game_category};
use Auth, DB, Hash, Validator, Session;
use Illuminate\Http\Request;
use App\Traits\UserTraits;
class ProfileController extends Controller
{
    use UserTraits;

    public function __construct()
    {
        $this->api = new ApiRepository;
        $this->custom = new CustomRepository;
    }

    public function index() {
        $data = $this->custom->getLayoutData();
        $balance = $this->balance("balance");
    	$data['user_balance'] = $balance;
    	$data['user_bank_name'] = Auth::user()->bankName();

        return view(config('global.viewspath') . '.menu.profile', $data);
    }
    /*
    *   ONly For WAP Version
    *
    */

    public function showChangePasswordWap($value='')
    {
        if(config('global.channel') != 'wap'){
            abort(404);
        }
        $data = $this->custom->getLayoutData();
        return view(config('global.viewspath') . '.menu.change_password', $data);
    }

    public function changePassword(Request $request){
        if( ! auth()->check()){
            if($request->expectsJson()){
                return response()->json(['status'=>300, 'message'=> trans('auth.unauthorized')] ); 
            }else{
                return response(backUrlHtml().trans('auth.unauthorized'));
            }
        }

        // ignore reset password
        if($request->has('submit_oldpass')){
            Log_change_pass::insert([
                    'web_id' => config('global.web_id'),
                    'user_id' => Auth::user()->user_id,
                    'old_password' => NULL,
                    'created_date' => date('Y-m-d H:i:s')
            ]);
            Session::put('reminderpass', 1);
            if($request->expectsJson()){
                return response()->json(['status'=>200, 'message'=> trans('auth.pass_not_chg')] ); 
            }else{
                return response(backUrlHtml().trans('auth.pass_not_chg'));
            }
        }

        $data = $request->all();

        $niceNames = [
            'old_password' => trans('passwords.old_password'),
            'new_password' => trans('passwords.new_password'),
            'new_password_confirmation' => trans('passwords.verify_password')
        ];

        $rules = [
            'old_password'                  => 'required|string',
            'new_password'                  => 'required|string|min:6|max:32',
            'new_password_confirmation'     => 'required|string|same:new_password',
        ];
        $validator = Validator::make($data, $rules);
        $validator->setAttributeNames($niceNames); 

        if($validator->fails()){
            if($request->expectsJson()){
                return response()->json(['status'=>301, 'message'=>$validator->errors()->first()]);
            }else{
                return response(backUrlHtml().$validator->errors()->first());
            }
        }

        $upass = auth()->user()->user_pass;
        if ( ! Hash::check($request->old_password, $upass)) {
            if($request->expectsJson()){
                return response()->json(['status'=>302, 'message'=> trans('auth.pass_chg_wrongpass')] ); 
            }else{
                return response(backUrlHtml().trans('auth.pass_chg_wrongpass'));
            }
        }

        $str = $request->new_password_confirmation;           
        if( ! (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) ){
            if($request->expectsJson()){
                return response()->json(['status'=>303, 'message'=> trans('auth.pass_chg_notvalid')] ); 
            }else{
                return response(backUrlHtml().trans('auth.pass_chg_notvalid'));
            }
        }

        if(strtoupper($str)== strtoupper(auth()->user()->user_name)){
            if($request->expectsJson()){
                return response()->json(['status'=>304, 'message'=> trans('auth.pass_chg_usersame')] ); 
            }else{
                return response(backUrlHtml().trans('auth.pass_chg_usersame'));
            }
        }
        
        if($str == $request->old_password){
            if($request->expectsJson()){
                return response()->json(['status'=>305, 'message'=> trans('auth.pass_chg_oldsame')] ); 
            }else{
                return response(backUrlHtml().trans('auth.pass_chg_oldsame'));   
            }
        }
        
        $uid = auth()->user()->user_id;
        $update = User::where('user_id', $uid)->update(['user_pass'=>Hash::make($request->new_password)]);
        if($update){ 
            $log_id=506; 
            $this->user_log($log_id); 
            Log_change_pass::insert([
                    'web_id' => config('global.web_id'),
                    'user_id' => Auth::user()->user_id,
                    'old_password' => $upass,
                    'created_date' => date('Y-m-d H:i:s')
            ]);
            Session::put('reminderpass', 1);
            if($request->expectsJson()){
                return response()->json(['status'=>200, 'message'=> trans('auth.pass_chg_success')]); 
            }else{
                return response(backUrlHtml().trans('auth.pass_chg_success'));
            }
        }else{
            if($request->expectsJson()){
                return response()->json(['status'=>306, 'message'=> trans('auth.pass_chg_fail')] ); 
            }else{
                return response(backUrlHtml().trans('auth.pass_chg_fail'));
            }

        }

    }

    public function balance($code){

        if(strtolower($code) == "balance") { 
            //newly login
            if(session('loggedin') || $this->checkUserLog(Auth::user()->user_name,514) ==0){
                $checkUserCoinLock = $this->checkUserCoinLock(Auth::user()->user_id);
                if($checkUserCoinLock == 1) { 
                    return '<i class="fa fa-circle-o-notch fa-spin"></i>'; 
                }
            }else{
                session(['loggedin' => 1]);
            } 
        }
        
        $alert=1;
        $res = $this->check_balance($code,$alert); 

        if(!empty($res['status']) && $res['status']==305 && $code != 'SBO' && $code != 'PTIM'){
            $search=array(
                        "user_id"       => Auth::user()->user_id,
                        "memo_isauto"   => 1,
                        );
            
        $exist=Memo::whereDate('memo_date', '=', date('Y-m-d'))->where($search)->first(['memo_id']);
 
            if(empty($exist)){
                $title = "User $res[provider] Error ( $res[api_status] )";
                $descr=$res['message'] . "<BR> Segera laporkan ke provider $res[provider] ! " ;
                $memo_group = $this->custom->get_user_memo_group(Auth::user()->user_name);

                $memoID = Memo::insertGetId([
                    "web_id"                => config('global.web_id'),
                    "operator_id"           =>  1,
                    "deleted_operator_id"   =>  "1",
                    "user_id"               =>  Auth::user()->user_id,
                    "memo_title"            =>  "**Auto Memo - $title**",
                    "memogroup_id"          =>  $memo_group,
                    "memo_descr"            =>  'PLAYER:: '.$descr.'::separator::',
                    "memo_isauto"           =>  "1",
                    "memo_date"             =>  date('Y-m-d H:i:s')
                ], 'memo_id');

                if($memoID){ 
                    $log_id=529;
                    $logs['title'] =  $title;
                    $logs['desc'] =  $descr ; 
                    $this->user_log($log_id, $logs); 
                }
            }
        }

        return $res;
    }

    public function turnover(){
        $data = $this->custom->getLayoutData();

        $db2 = config('database.connections.db_config.database').'.';
        $joinTransDay = Join_trans_day::from('join_trans_day as jtd')
            ->join($db2.'config_game as cg', 'jtd.game_id', '=', 'cg.game_id')
            ->select('jtd.created_date', 'cg.game_category', DB::raw('sum(tover) as total_to, sum(pl_comms) as pl_comms'))
            ->where('cg.vendor_id', '<>', 16)
            ->where('user_id', Auth::user()->user_id)
            ->where('jtd.created_date', '>=', DB::raw('DATE(NOW()) - INTERVAL 2 week'))
            ->orderByDesc('jtd.created_date')
            ->take(10);
        
        $data['turnOver'] = $joinTransDay->groupBy(DB::raw("DATE_FORMAT(jtd.created_date,'%Y-%m-%d')"))->get();
        $data['turnOverDetail'] = $joinTransDay->groupBy(DB::raw("cg.game_category"))->get();

        if($data['turnOverDetail']->count()){
            $to_data = array();
            $to_cate = array();

            $cfgGameCate = Config_game_category::get();
            $game_cate = array();
            foreach($cfgGameCate as $v_cate){
                $game_cate[$v_cate["game_category"]] = $v_cate["name"];
            }
            
            foreach($data['turnOverDetail'] as $key => $r2){
                $prev="";
                if($key > 0)
                    $prev = date("Y-m-d",strtotime($data['turnOverDetail'][$key-1]["created_date"]));

                if($prev == date("Y-m-d",strtotime($r2["created_date"]))){
                    $to_cate[] = $game_cate[$r2['game_category']];
                    $to_data[$currkey][$game_cate[$r2['game_category']]] = array("to" => $r2['total_to'], 'plcomms' => $r2['pl_comms']);

                }else{
                    $to_cate[] = $game_cate[$r2['game_category']];
                    $currkey = date("Y-m-d",strtotime($r2["created_date"]));// 1
                    $to_data[$currkey][$game_cate[$r2['game_category']]] = array("to" => $r2['total_to'], 'plcomms' => $r2['pl_comms']);
                }
                
            }
            $to_cate = array_unique($to_cate);
            sort($to_cate);

            $data['to_data'] = $to_data;
            $data['to_cate'] = $to_cate;
        }

        return view(config('global.viewspath') . '.menu.turnover_info', $data);
    }

    public function turnoverhkb() {
        $url = $this->api->get_game_link('HKB');
        if($url){
            $url .= '&infotab=list_turnover';
        }
        return redirect($url);   
    }

}
