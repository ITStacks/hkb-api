<?php

namespace App\Repositories;
use App\Models\{Adm_transfer,Join_balance,Join_lastorder, Log_api, Log_user, User, User_coin, User_active, Settings_image};
use App\Models\DbConfig\{Adm_config, Config_vendor,Config_game, Config_lobby_game, Partner_vendor};

use App\Repositories\API\CommonWallet\CommonWalletRepository;

use Auth,DB,Exception,Log;

use App\Traits\UserTraits;

class ApiRepository 
{ 
    use UserTraits;

    protected static $config_vendor_hkb = [];
    protected $config_vendor_active = [];

    public function __construct($web_id=0)
    { 
        try {            
            if(!(DB::connection()->getDatabaseName()))throw new Exception('failed get initial response');
        }catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration.");
        }

        if(!count(static::$config_vendor_hkb)){
            if(empty(config('global.web_id'))){
                config(['global.web_id'        => $web_id]);
            }

            $vendor_ids=array();
            $vendor_ids=Partner_vendor::where([
                        'web_id'     => config('global.web_id'),
                        'status'     => 1,
                        ])->pluck('vendor_id');
              
            //005hkbg,003 ddc,016hkb
            $r=Config_vendor::from('config_vendor as cv') 
                       ->join('vendors as v', 'v.vendor_id', '=', 'cv.vendor_id')
                       ->whereIn('cv.vendor_id',$vendor_ids)
                       ->orderBy('cv.vendor_order')
                       ->get(['cv.vendor_id','cv.vendor_name',  'cv.vendor_prefix', 'cv.vendor_operator_id', 'cv.vendor_key','cv.vendor_alias', 'cv.vendor_api_url', 'cv.vendor_lobby_url','cv.maint_api','cv.maint_lobby','cv.conversion','cv.min_transfer', 'cv.max_transfer', 'cv.vendor_options', 'cv.vendor_order', 'cv.dp_block', 'cv.wd_block', 'v.wallet_type', 'v.is_apibridge']);
 
            $array=array();
             foreach($r as  $detail){
                    //$array[$detail->vendor_id]['vendor_name']=$detail->vendor_name;
                    $array[$detail->vendor_name]['vendor_id']=$detail->vendor_id;
                    $array[$detail->vendor_name]['vendor_name']=$detail->vendor_name;
                    $array[$detail->vendor_name]['vendor_prefix']=$detail->vendor_prefix;
                    $array[$detail->vendor_name]['vendor_operator_id']=$detail->vendor_operator_id;
                    $array[$detail->vendor_name]['vendor_key']=$detail->vendor_key;
                    $array[$detail->vendor_name]['vendor_alias']=$detail->vendor_alias;
                    $array[$detail->vendor_name]['vendor_api_url']=$detail->vendor_api_url;
                    $array[$detail->vendor_name]['maint_api']=$detail->maint_api;
                    $array[$detail->vendor_name]['vendor_lobby_url']=$detail->vendor_lobby_url;
                    $array[$detail->vendor_name]['maint_lobby']=$detail->maint_lobby;
                    $array[$detail->vendor_name]['min_transfer']=$detail->min_transfer;
                    $array[$detail->vendor_name]['max_transfer']=$detail->max_transfer; 
                    $array[$detail->vendor_name]['conversion']=$detail->conversion;
                    $array[$detail->vendor_name]['vendor_options']=$detail->vendor_options;
                    $array[$detail->vendor_name]['vendor_order']=$detail->vendor_order;
                    $array[$detail->vendor_name]['dp_block']=$detail->dp_block;
                    $array[$detail->vendor_name]['wd_block']=$detail->wd_block;
                    $array[$detail->vendor_name]['wallet_type']=$detail->wallet_type;
                    $array[$detail->vendor_name]['is_apibridge']=$detail->is_apibridge;

            }
    
            static::$config_vendor_hkb =$array;

        }

        if(isset(static::$config_vendor_hkb)){
            $this->config_vendor_active=static::$config_vendor_hkb;
        }

        $this->common_wallet = new CommonWalletRepository;
        foreach ($this->config_vendor_active as $key => $value) {
            if($key == "SBO"){
                $key = "TRG";
            }
            
            if(!in_array($key,['SPA'])){ //Please include all common wallet using CW that doesn't have own Repository 
                $class = "App\Repositories\API\\".strtoupper($key)."_Repository";  
                if(in_array($key,['WIN568','AGIN','VG','TTG','HG','SAG','PP'])){ //Special case CW but using its own Repo
                    $class = "App\Repositories\API\CommonWallet\\".strtoupper($key)."_Repository";  
                } 

                $var = strtolower($key);                             
                try {
                    $this->$var = new $class;                            
                } catch (\Throwable $th) {                
                    //Catch vendor that is active but not integrated and without Repo.
                }                             
            }         
        }       
    }

    public function getActiveVendorList(){
        return $this->config_vendor_active;
    }

    public function get_lobby_link()
    {
        $vendors = $this->config_vendor_active;
        $vendor_code = [];
        foreach ($vendors as $key => $vendor) {
            $vendor_code[] = $vendor['vendor_id'];
        }

        $providerGames = Config_lobby_game::join('lobby_game as lg', 'config_lobby_game.id', '=', 'lg.config_lobby_game_id')
                                            ->where('lg.status', '!=', 0)
                                            ->where('lg.web_id', config('global.web_id'))
                                            ->where('game_lobby',1)
                                            ->where('game_isactive', 1)
                                            ->whereIn('vendor_id', $vendor_code)
                                            ->orderBy('lg.game_order')
                                            ->get(['game_click','vendor_id','config_lobby_game.id','game_type','game_id','game_name','vendor_id', 'game_id_post', 'lg.status', 'lg.icon_path_desktop', 'lg.icon_path_mobile', 'lg.icon_path_dropdown', 'lg.game_order','lg.promo_status']);

        $status_user_message = $this->status_user_message();

        foreach ($vendors as $key => $vendor) {
            $others = false;
            $maintenance = $vendor['maint_api'];
            $vendor_name = $vendor['vendor_name']; 
            $vendor_alias = $vendor['vendor_alias']; 
            $games = $providerGames->where('vendor_id', $vendor['vendor_id']);

            $url_name = 'array_url_lobby';
            if($vendor_name != config('global.code_hkb')){
                $others = true;
            }

            foreach($games as $k => $game){
                $game_id = $game->game_id;
                $config_lobby_game_id = $game->id;
                $url_category = strtolower(str_replace(' ', '', $game->game_type));
                if(strtolower($game->game_type) == 'card'){
                    $url_category = 'cardgames';
                }else if(strtolower($game->game_type) == 'togel'){
                    $url_category = 'lottery';
                    $config_lobby_game_id = $game_id;
                }
                $route_show = $url_category;
                if(strtolower($game->game_type) == 'e-games'){
                    $route_show = 'egames';
                }
                
                if($vendor_name == 'HKB'){
                    $game_id = strtolower($game->game_id_post);
                }else if($vendor_name == 'SBO'){
                    $game_id = strtolower($game->game_id_post);
                }

                $onclick_value = 'javascript:void(0)';
                $data[$url_name][$url_category][$config_lobby_game_id] = [
                        "url" => 'javascript:void(0)',
                        'icon_desktop' => $game->icon_path_desktop,
                        "icon_mobile" => $game->icon_path_mobile,
                        "icon_dropdown" => $game->icon_path_dropdown,
                        "display_game_id" => $game_id,
                        "game_click" => $game->game_click,
                        "game_type" => $game->game_type,
                        "name" => str_ireplace('togel', '', $game->game_name),
                        "order" => $game->game_order,
                        "onclick" => $onclick_value,
                        "maintenance" => $maintenance,
                        "vendor_id" => $game->vendor_id,
                        "vendor_name" => $vendor_name,
                        "promo" => $game->promo_status,
                        "id"    => $game->id
                    ];

                if (Auth::check()){
                    if($maintenance == 1){
                        if(config('global.channel') == "wap"){
                            $onclick_value = route('wap.response', [1, $vendor['vendor_alias']]);
                        }else{
                            $onclick_value = "uialert('". trans('lang.lobby_maintenance', ['destination'=>$vendor['vendor_alias']]) . "')";
                        }
                    }else{
                        if($status_user_message){
                            if(config('global.channel')=="wap"){
                                $onclick_value = route('wap.response', [2, $status_user_message]);
                            }else{
                                $onclick_value = "uialert('".$status_user_message."')";
                            }
                        }else{
                            if($game->status == 2){
                                if(config('global.channel')=="wap"){
                                    $onclick_value = route('wap.response', [3,'Coming Soon...']);
                                }else{
                                    $onclick_value = "uialert('Coming Soon...')";
                                }
                            }else{
                                // open lobby game link
                                $open_game_id = $game->game_id;
                                if(config('global.channel')=="wap"){
                                    $parameter = [4, $vendor['vendor_name']];
                                    if($vendor_name != 'HKB'){
                                        $parameter['game'] = $game_id;
                                    }
                                    $onclick_value = route('wap.response', $parameter);
                                }else{
                                    $onclick_value = "open_lobby_vendor('".$vendor['vendor_name']."','".$open_game_id."')";
                                    if(strtolower($game->game_type) == 'e-games'){
                                        $onclick_value = 'location.href = "'.route('egames.show',str_replace(' ','',$vendor_alias)).'"';
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if(strtolower($game->game_type) == 'e-games'){
                        $onclick_value = 'location.href = "'.route('egames.show',str_replace(' ','',$vendor_alias)).'"';
                    }else{
                        $data[$url_name][$url_category][$config_lobby_game_id]['url'] = route($route_show.'.show', strtolower(str_replace(' ', '-', $game->game_name)));
                    }
                }
                $data[$url_name][$url_category][$config_lobby_game_id]['onclick'] = $onclick_value;
                    
                if(strtolower($game->game_type) == 'togel'){
                    $linktg = "#";
                    $link_result_tg = '#';
                    $link_result_tg=route('result', ['gamecode' => strtoupper($game_id)]);
                    if (strtoupper($game_id)=="SG"){
                    	$linktg="http://www.singaporepools.com.sg";
                    }
                    else if (strtoupper($game_id)=="MC") {
                    	$linktg="http://www.magnumcambodia.com/";
                    }
                    else if (strtoupper($game_id)=="SD") {
                    	$linktg="http://www.sydneypoolstoday.com/";
                    }
                    else if (strtoupper($game_id)=="CN") {
                    	$linktg="http://www.chinapools.asia/";
                    }
                    else if (strtoupper($game_id)=="TW") {
                    	$linktg="http://www.taiwanlottery.net/";
                    }
                    else if (strtoupper($game_id)=="HK") {
                    	$linktg="http://www.hongkongpools.com";
                    }
                    else if (strtoupper($game_id)=="JPN") {
                        $linktg="http://www.japanpools.online";
                    }
                    $data[$url_name][$url_category][$config_lobby_game_id]['pools_url'] = $linktg;
                    $data[$url_name][$url_category][$config_lobby_game_id]['result_url'] = $link_result_tg;
                }

                if($others && isset($data[$url_name][$url_category])){
                    if(isset($data[$url_name]['other'])){
                        $data[$url_name]['other'] += $data[$url_name][$url_category];
                    }else{
                        $data[$url_name]['other'] = $data[$url_name][$url_category];
                    }
                }
            }

        }
        if(isset($data['array_url_lobby'])){
            $sort_col = array();
            foreach ($data['array_url_lobby'] as $key=> $row) {
                foreach ($row as $row_key => $row_value) {
                    if(!isset($row_value['order'])){
                        break;
                    }
                    $sort_col[$key] = $row_value['order'];
                }
                // sort games order inside the category
                uasort($data['array_url_lobby'][$key], function($a, $b) {
                    return $a['order'] <=> $b['order'];
                });
            }
            // sort which category games goes first
            if(!empty($sort_col)){
                array_multisort($sort_col, SORT_ASC, $data['array_url_lobby']);
            }
            $data['array_url_lobby_ori'] = $data['array_url_lobby'];
        }
        $data['url_hkb'] = route('register');
        if(Auth::check()){
            $data['url_hkb'] = 'javascript:void(0)';
        }
        //json1($data['array_url_lobby']);exit;
        // dd($data);
        return $data;
    }

    public function getReferral($start_date, $end_date)
    {
        $data = $this->hkb->referralData(Auth::user(), $start_date, $end_date);
        if($data['status'] == 200){
            $data_referral = $data['data_referral'];
        }else{
            $data_referral = [];
        }
        return $data_referral;
    }

    public function getTurnover($start_date, $end_date)
    {
        /*$data = $this->hkb->referralData(Auth::user(), $start_date, $end_date);
        if($data['status'] == 200){
            $data_referral = $data['data_referral'];
        }else{
            $data_referral = [];
        }
        return $data_referral;*/
    }
    
    //====HKBG
    public function get_ddc_link(){
        return $this->hkbg->get_ddc_link();
    }
    public function get_tg_link(){
        return $this->hkbg->get_tg_link();
    }
    public function get_hkbg_link(){
        return $this->hkbg->get_hkbg_link();
    }
    public function get_hkbg_jackpot(){ 
        return 0;
        return $this->hkbg->get_hkbg_jackpot();
    }
    public function get_hkb_link(){
        return array();
        return $this->hkbg->get_hkb_link();
    }

    public function get_number_details($game_id, $room_id, $period)
    {
        return $this->hkb->get_dingdong_detail($game_id, $room_id, $period);
    }

    public function get_hkb_jackpot(){ 
        return $this->hkb->get_jackpot();
    }

    public function transfer($vendor_name,$arr){
        if(!isset($this->config_vendor_active[$vendor_name])){
            return 0;
        }
        $vendor = $this->config_vendor_active[$vendor_name];
        $dp_block = $vendor['dp_block'];
        $wd_block = $vendor['wd_block'];

        if($dp_block && $arr['to']) {
            return 0;
        }
        if($wd_block && $arr['from']) {
            return 0;
        }

        if(Auth::user()){
            $user=Auth::user();
            $user_id=Auth::user()->user_id;
        }else{
            $user_id=$arr['user_id'];
            $user=User::where('user_id',$user_id)->first(); 
        }
        $transfer_id= Adm_transfer::where([
                ['user_id','=', $user_id],
                ['status','<',1]
                    ])->first(['status']);
        if(!empty($transfer_id)){
            if($transfer_id->status == 0) return 0;
            return -1;
        }

        $done_amount=0;
        switch($vendor_name){ 
            CASE "HKBG"://HKBG Balance
            CASE "DDC"://DDC Balance
                    $done_amount= $this->hkbg->transfer($vendor_name,$arr);    
                    break;
            CASE "HKB"://HKB2 Balance
                    $done_amount= $this->hkb->transfer($user,$arr);    
                    break;
            CASE "TRG":
                    $done_amount= $this->trg->transfer_casino($user,$arr);    
                    break;
            CASE "SBO":
                    $done_amount= $this->trg->transfer_sportsbook($user,$arr);    
                    break;        
            CASE "PTIM":
                    $done_amount= $this->ptim->transfer($user,$arr);   
                    break;
            CASE "BPG": //BPG Balance
                    $done_amount= $this->bpg->transfer($user,$arr);
                    break;
            CASE "PP":            
            CASE "WIN568":
            CASE "SAG":
            CASE "HG":
            CASE "AGIN":
            CASE "TTG":
            CASE "VG":
            CASE "SPA":
                    $done_amount= $this->common_wallet->transfer($user,$arr);
                    break;
            CASE "OGP":
                $done_amount= $this->ogp->transfer($user,$arr);
                break;     
            CASE "CMD":
                $done_amount= $this->cmd->transfer($user,$arr);
                break;     
        }

        if($done_amount>1)$done_amount=1;

        return $done_amount;
        
    }

    public function get_vendor_name($vendor_id)
    {
        foreach($this->config_vendor_active as $key => $detail){
            if($detail['vendor_id']==$vendor_id) return $detail['vendor_name'];
        }

    }


    public function check_transfer($vendor_id,$trans_id,$transfer_details){     

        $vendor_name = $this->get_vendor_name($vendor_id); 

        switch($vendor_name){ 
            CASE "HKBG"://HKBG Balance
            CASE "DDC"://DDC Balance
                    return $this->hkbg->check_trans($vendor_name,$trans_id);    
                    break;
            CASE "HKB"://HKB2 Balance
                    return $this->hkb->check_trans($trans_id,$transfer_details);    
                    break;
            CASE "TRG":
                    return $this->trg->check_trans_casino($trans_id,$transfer_details);    
                    break;
            CASE "SBO":
                    return $this->trg->check_trans_sportsbook($trans_id,$transfer_details);    
                    break;            
            CASE "PTIM":
                    $user = Auth::user();
                    return $this->ptim->check_trans($user,$trans_id,$transfer_details);  
                    break;  
            CASE "BPG":
                    return $this->bpg->check_trans($trans_id,$transfer_details);
                    break;
            CASE "PP":
            CASE "HG":
            CASE "WIN568":
            CASE "SAG":
            CASE "AGIN":
            CASE "TTG":
            CASE "VG":
            CASE "SPA":
                    return $this->common_wallet->check_trans($trans_id, $transfer_details);
                    break;
            CASE "OGP":
                    return $this->ogp->check_trans($trans_id,$transfer_details);
                    break;  
            CASE "CMD":
                    $user = Auth::user();
                    return $this->cmd->check_trans($trans_id,$user, $transfer_details);
                    break;              
        }
    }

    //1 success
    //2 canceled
    //3 refund
    //4 correction out
    //5 onhold
    public function check_refund($transfer_details){

        try {            
            if(!(DB::connection()->getDatabaseName()))throw new Exception('failed get database');
        }catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration.");
        } 

            $transfer_dir = $transfer_details->type; 

            $transfer_id = $transfer_details->transfer_id; 
            $vendor_id = $transfer_details->vendor_id; 
            $vendor_name = $transfer_details->vendor_name;
            $point = $transfer_details->conversion;      
            $transfer_amount = $transfer_details->amount; 

            $transfer_amount = $transfer_amount*$point;

            $user_id=Auth::user()->user_id;
            $user_name=Auth::user()->user_name; 

            $transferDate=date("Y-m-d 00:00:00",strtotime($transfer_details->date));

            $end_date = $transferDate;
            $start_date = date('Y-m-d',(strtotime ( '-3 month' , strtotime ( $end_date) ) ));
            $quarter = getQuartalPartitionRange($start_date, $end_date);

            $search=array( 
                    "user_id"=>$user_id,
                    "trans_id"=>$transfer_id,
                    );

            $statement= Join_balance::from(DB::raw('join_balance PARTITION('.$quarter.')'))
                        ->where($search)
                        ->whereBetween('datetime',[$transferDate,date("Y-m-d 23:59:59")])
                        ->orderBy('id','DESC')->first(['act','amount']);
            
        switch($transfer_dir){
            CASE 1:
                    $status= 2;// canceled
                    if(!empty($statement)){ 
                        if( $statement->act==4 && $statement->amount == $transfer_amount ){ 
                            $status=3; 
                        }
                    } 
                    break;
            CASE 0:
                    $status= 1;//success 
                    if(empty($statement)){
                        $status=3; // refund player    
                    } 
                    break;

            default:$status= 5;break;
        }  
        
        if ($status==3){
            
            $current_balance=$this->getCurrentUserBalance($user_id);
        
            $arr_update=array(); 
            $current_balance=$current_balance+$transfer_amount; 
                                        
            $arr_update['act']=3;
            $arr_update['user_id']=$user_id;
            $arr_update['user_name']=$user_name;
            $arr_update['current_balance']=$current_balance;
            $arr_update['amount']=$transfer_amount;
            $arr_update['trans_id']=$transfer_id;
            $arr_update['vendor_id']=0;
            $arr_update['vendor_name']=$vendor_name;
            $arr_update['descr']="SYSTEM $vendor_name";

            $this->update_api_bal_statement($arr_update);

        }
        return $status;
    }


    public function check_correction($transfer_details){

        try {            
            if(!(DB::connection()->getDatabaseName()))throw new Exception('failed get database');
        }catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration.");
        } 

            $transfer_dir=$transfer_details->type; 

            $transfer_amount=$transfer_details->amount; 
            $transfer_id=$transfer_details->transfer_id; 
            $vendor_id=$transfer_details->vendor_id; 
            $vendor_name = $transfer_details->vendor_name;
            $point = $transfer_details->conversion;     

            $transfer_amount=$transfer_amount*$point;  

            $user_id=Auth::user()->user_id;
            $user_name=Auth::user()->user_name;

            $search=array( 
                    "user_id"=>$user_id,
                    );

            $statement= Join_lastorder::onWriteConnection()->where($search)->orderBy('join_id','DESC')->first(['act','amount']);  
            
            if(!empty($statement))$statement_amount=$statement->amount; 

        switch($transfer_dir){
            CASE 1:
                    if(empty($statement))return 1; // success
                 
                      // act 4 transfer out
                    $current_balance=$this->getCurrentUserBalance($user_id);
                    if($statement->act!=4){
                        //4:#Transfer Out
                        $arr_update=array(); 
                        $current_balance=$current_balance-$transfer_amount; 
                        if($current_balance<0)$current_balance=0;
                                                 
                        $arr_update['act']=4;
                        $arr_update['user_id']=$user_id;
                        $arr_update['user_name']=$user_name;
                        $arr_update['current_balance']=$current_balance;
                        $arr_update['amount']=$transfer_amount;
                        $arr_update['trans_id']=$transfer_id;
                        $arr_update['vendor_id']=0;
                        $arr_update['vendor_name']=$vendor_name;
                        $arr_update['descr']="SYSTEM $vendor_name";
     
                        $this->update_api_bal_statement($arr_update);

                        return 4; // correction out player 
                    }
                    return 1; // success
                    break;
            CASE 0: 
                    break;

            default:return 5;break;
        }  


    }
    public function getCurrentUserBalance($user_id){
        $coin =User_coin::getBalance($user_id,0);
        if($coin<0){
            throw new Exception('Error balance problem UID: '. $user_id."; coin : ".$coin);
        }  
        return $coin;
    }

    public function getCurrentTotalBalance($user_id,$pull_force=0,$arr_to=array()){

        $query=User_coin::onWriteConnection()->where('user_id',$user_id)->get(['vendor_id','coin']);
  
        $coin = 0;
        foreach($query as $row){    

            if($row->vendor_id==0) {
                $coin += $row->coin; 
                continue;
            }
            $vendor_name = $this->get_vendor_name($row->vendor_id); 
            
            if(isset($this->config_vendor_active[$vendor_name])){
                $config_vendor = $this->config_vendor_active[$vendor_name];
            }else{
                // if vendor not active
                continue;
            }
            
            if($config_vendor["wallet_type"]=="TW"){
                $bal[$vendor_name] = self::check_balance($vendor_name);
            }elseif($config_vendor["wallet_type"]=="CW"){
                $bal[$vendor_name] = $row->coin;
            } 

            if(!is_numeric($bal[$vendor_name])){
                continue;
            }
            if($bal[$vendor_name] > 0) { 
                //continue on transfer to same provider 
                if(isset($arr_to['to']) && ($arr_to['to'] == $vendor_name) ){
                    continue;
                } 
                if($config_vendor["maint_api"]==0){
                    $coin += $bal[$vendor_name] * $config_vendor["conversion"] ; 
                    if($pull_force == 1){
                        $arr_from['type']=0; 
                        $arr_from['amount']=$bal[$vendor_name]; 
                        $arr_from['vendor_name'] = $vendor_name;
                        $arr_from['from']= 'ALL';
                        $this->transfer($vendor_name, $arr_from);                        
                    }
                }
            } 
        } 
        return $coin;
    }

    public function check_balance($vendor_name,$echo=0){
        if($vendor_name != 'balance' && !isset($this->config_vendor_active[$vendor_name])){
            return 0;
        }
        //0:Blocked,1:Active,2:NotYetValidate,3:FailValidate,4:Trouble
        if( ! Auth::check()) 
            return ['status'=>300, 'message'=>'Username not valid.'];
        $user=Auth::user();
        $user_id=$user->user_id;

        if($vendor_name !='balance'){
            if($user->status!=1) return 0;
            if($this->config_vendor_active[$vendor_name]["maint_api"]){
                return 0;
            }
        }

        switch($vendor_name){
            CASE "balance"://Main Balance 
                    // $bal= User_coin::getBalance($user_id,0);
                    $bal=$this->getCurrentTotalBalance($user_id);
                    if ($bal < 0) return "Error Balance";   
                    return number_format($bal);
                    break; 
            CASE "HKB"://HKB2 Balance
                    $bal = $this->hkb->balance($user,$echo);   
                    break; 
            CASE "TRG"://TRG Balance
                    $bal = $this->trg->balance_casino($user, $echo); 
                    break; 
            CASE "SBO":
                    $bal = $this->trg->balance_sportsbook($user, $echo); 
                    break;             
            CASE "PTIM":
                    $bal = $this->ptim->balance($user, $echo); 
                    break; 
            CASE "BPG": // BPG Balance
                    $bal = $this->bpg->check_balance($user, $echo);   
                    break;          
            CASE "PP":
            CASE "HG":
            CASE "WIN568":
            CASE "SAG":
            CASE "AGIN":
            CASE "TTG":
            CASE "VG":
            CASE "SPA":
                    $bal = $this->common_wallet->balance($vendor_name, $user, $echo); 
                    break; 
            CASE "OGP":
                $bal = $this->ogp->balance($user, $echo);     
                break;    
            CASE "CMD":
                $bal = $this->cmd->balance($user, $echo);   
                break;            
            default:
                return 0;
                break;
        } 

        return $bal;
    }
    
    public function getUserCoin($vendor_id, $user_id,$echo=0)
    {
        $coin =User_coin::getBalance($user_id,$vendor_id); 
        if($coin<0){
            if($echo==1) {
                return $coin;
            }else{
                $sportsbook_vendor_id=[21];
                if(in_array($vendor_id, $sportsbook_vendor_id)){
                    // to ignore minus balance on sportsbook
                    $coin=0;
                }else{
                    throw new Exception('Error balance problem user_id '.$user_id." vendor_id :".$vendor_id);  
                }
            } 
        } 
        return $coin;
    }
    public function get_game_link($vendor_name,$game_id=""){
        if(!isset($this->config_vendor_active[$vendor_name])){
            return 0;
        }
        $vendor = $this->config_vendor_active[$vendor_name];

        $maintenance = $vendor['maint_api'];
        if($maintenance == 1) {
            return 0;
        }
        if($vendor['maint_lobby']) return 0;

        $home_url="";

        if( isWap() || (!empty(config('global.apk_add_home_url'))) ) {
            $home_url=getSiteUrl();
        }

        $sess = session('user_lastloginsession');//Auth::user()->last_session;

        if(Auth::check()) {
            $user=Auth::user();
            $active_session=User_active::where("user_id", $user->user_id)->first(['sess_id']);
            if(!empty($active_session))$sess=$active_session->sess_id;
        }

        $game_id_field = 'game_id_mobile';
        if(get_channel_id()==1){
            $game_id_field = 'game_id';
        }

        switch($vendor_name){
            CASE "HKB":
                $this->hkb->getGameToken($user);
                $game_token = session('hkb_token');
                $method = "/api/lobby";

                $url = $vendor['vendor_lobby_url']."$method?&gt=".$game_token."&game_id=".$game_id."&home_url=".$home_url;
                
                if(config('global.channel') == 'wap')$url .= "&channel=wap";
                
                break;
            CASE "WIN568":
                session(['enter_win568_game' => '1']);
                $url = $this->win568->getGameUrl($user,$game_id);
                break;
            CASE "TRG":
            CASE "SBO":
                session(['enter_sbo_game' => '1']);
                $url = $this->trg->game_url($game_id);
                break;                    
            CASE "PTIM":
                if(!Auth::check()){
                    $user = null;
                }                
                $url = $this->ptim->game_url($game_id, $user);
                break;
            CASE "BPG":
                session(['enter_bpg_game' => '1']);
                $url = $this->bpg->login($user, $game_id);
                break;
            CASE "PP":
                $url = $this->pp->game_url($user, $game_id);
                /*$check_game = Config_lobby_game::where($game_id_field, $game_id)->first(); 
                if(empty($check_game) && !isWap()) return 0;
                $platform = 'WEB';
                if(get_channel_id()==2){
                    $platform = 'MOBILE';
                }
                $technology = 'H5';
                $flash_game_id = ['vpa', 'kna'];
                if(in_array($game_id, $flash_game_id) && get_channel_id() != 2){
                    $technology = 'F';
                }
                $wl_format_token = Adm_config::where('id', 46)->first();
                $pp_arr = array('token' => str_replace('_', '', $vendor['vendor_prefix']).$wl_format_token->value.$sess,
                                'symbol' => $game_id, //bjma
                                'technology' => $technology,
                                'platform' => $platform,
                                'language' => 'id',
                                'cashierUrl' => route('deposit'),
                                'lobbyUrl' => route('home'));
                $key = urlencode(http_build_query($pp_arr));
                $url = $vendor['vendor_lobby_url']."/gs2c/playGame.do?key=".$key."&stylename=".$vendor['vendor_operator_id'];*/
                break;
            CASE "SPA":
                if(!Auth::check()){
                    $acct_id = "DemoTestPlayer";
                    $forFun = true;                    
                } else {                    
                    $acct_id = $vendor['vendor_prefix'].$user->user_name;
                    $forFun = false;
                }

                // $check_game = Config_lobby_game::where($game_id_field,$game_id)->first();
                
                // if(empty($check_game) && !isWap()){
                //       return 0;
                // }
                $isMobile = (get_channel_id() != 1) ? "true": "false";                 

                $spa_arr = array('acctId' => $acct_id,
                               'token' => $sess,
                               'mobile' => $isMobile,
                               'game' => $game_id,
                               'fun' => $forFun);       

                $parameters = urldecode(http_build_query($spa_arr));   
                $url = $vendor['vendor_lobby_url']."/".$vendor['vendor_key'].'/auth/?'.$parameters;
                break;
            CASE "HG":
            //https://<hostname>/login/visitor/cwlogin.jsp?sessionid=ruqteihkgwkq81vjqwxytzqf&version=V3&lang=en&gameType=0000000000000004&skinId=SKIN001&exiturl=<exiturl address>
                $res = $this->hg->request_login($user);
                if(isset($res['maintenance']) || !isset($res['sessionid'])) return "";
                $check_game = Config_lobby_game::where($game_id_field, $game_id)->first(); 
                if(empty($check_game) && !isWap()) return 0;
                $url = $vendor['vendor_lobby_url']."?sessionid=".$res['sessionid']."&lang=en&gameType=".$game_id."&skinId=SKIN001";//&exiturl=".route('home')
                break;
            CASE "AGIN":
                $url = $this->agin->game_url($vendor_name,$game_id,$game_id_field);
                break;  
            CASE "SAG":
                $url = $this->sag->game_url($user);
                break;
            CASE "TTG":
                $url = $this->ttg->game_url($game_id,$user);
                break;
            CASE "VG":
                $url = $this->vg->game_url($vendor_name,$game_id,$game_id_field);
                break;
            CASE "OGP":
                $url = $this->ogp->game_url($user,$game_id,$game_id_field);
                break;
            CASE "CMD":
                session(['enter_cmd_game' => '1']);
                $url = $this->cmd->game_url($user, $game_id);
                break;                    
            default:
                $url=$home_url;
                break;
        }
        return $url;
    }

    public function register_user_to_provider(){ 
        
        $vendors=$this->config_vendor_active;
        foreach($vendors as $vendor_name =>$detail){
            $this->check_balance($vendor_name); 
        }  
    
    }


    // API-function CURL
    public function getCURL($vendor_name, $url, $info, $postdata = array(), $web_id = ''){

        if( !isset($_SERVER['HTTP_USER_AGENT']) ){
            throw new Exception('Curl error');
        }

        $is_apiBridge = isset($info['is_apiBridge']) ? $info['is_apiBridge'] : 0;

        // set_time_limit(0);
        $params   = is_array($postdata) ? urldecode(http_build_query($postdata)) : $postdata;  
        $method   = $info['method'];
        $log0     = $info['log_in'];
        $log1     = $info['log_out'];
        $trace_log = env('TRACE_LOG', 0);

         //  $params= htmlentities($params);
        if($log0 == 1){
            //CallLog::record_call($vendor,$method,$postdata,$log0,$web_id);
            $request_url=$url."?".$params;
            $this->record_api_call($vendor_name, $log0, $method, $request_url, $web_id);
        }
        config(['start_time' => microtime(true)]);
        config(['req_id' => identifier(10)]);
        if($trace_log){
            Log::stack(['daily-custom'])->info(json_encode(['req', config('req_id'), config('global.subweb_server'), $method, $params]));
        }
        
        $timeout=60.2*1000;    
        // for HKB and NOT PT provider
        if( !in_array($vendor_name,["PT","WIN568","OGP","PTIM","CMD","AGIN","SAG","HG","OGP","PP"]) && !$is_apiBridge){
            $options = array(
                            CURLOPT_URL            => $url,
                            CURLOPT_USERAGENT      => $_SERVER['HTTP_USER_AGENT'],
                            CURLOPT_NOSIGNAL       => 1,
                            CURLOPT_TIMEOUT_MS     => $timeout,
                            CURLOPT_FOLLOWLOCATION => TRUE,
                            CURLOPT_AUTOREFERER    => TRUE,
                            CURLOPT_RETURNTRANSFER => TRUE,                 
                            CURLOPT_POST           => TRUE,
                            CURLOPT_POSTFIELDS     => $params,  
                            CURLOPT_SSL_VERIFYPEER => TRUE,
                            
                            //OG            

                            //CURLOPT_HEADER => FALSE,
                            //CURLOPT_MAXREDIRS => 10,
                            //CURLOPT_ENCODING => '',
                            //CURLOPT_CONNECTTIMEOUT => 120,
                            //CURLOPT_SSL_VERIFYPEER => FALSE,
                            //CURLOPT_SSL_VERIFYHOST => FALSE,
                        );

        }else{
            $options = array(
                            CURLOPT_URL            => $url,
                            CURLOPT_HTTPHEADER     => ["Content-type: application/json",                                                                                
                            'Content-Length: ' . strlen(json_encode($postdata))],
                            CURLOPT_AUTOREFERER    => $_SERVER['HTTP_USER_AGENT'],
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_NOSIGNAL       => 1,
                            CURLOPT_TIMEOUT_MS     =>  $timeout,
                        );

            if( ! empty($postdata)){
                // $options[CURLOPT_CUSTOMREQUEST] = "POST";
                $options[CURLOPT_POST]       = TRUE;
                $options[CURLOPT_POSTFIELDS] = json_encode($postdata);
            }
        }

        if(isset($info['header']) && $info['header']){
            $options[CURLOPT_HTTPHEADER] = $info['header'];
        }
             
        $ch = curl_init();

        curl_setopt_array($ch, $options);

        $result = curl_exec($ch);

        if(curl_errno($ch)){
            echo 'Curl error: ' . curl_error($ch);
            $result .=curl_error($ch);
        }

        curl_close($ch);
        
        if($log1 == 1){

            $detail['req']=$url."?".$params;
            $detail['res']=$result;
            $detail['server']=getServerIP();
            $response=json_encode($detail);
            $this->record_api_call($vendor_name, $log1, "res-".$method, $response , $web_id);
        }
        if($trace_log){
            Log::stack(['daily-custom'])->info(json_encode(['res', config('req_id'), config('global.subweb_server'), $method, microtime(true) - config('start_time'), substr($result, 0, 5000)]));
        }

        return $result;
    } // end getCURL


    public function record_api_call($vendor_name,$direction, $method, $detail){

        // Log_api::new_log($vendor_name,$direction, $method, $detail);
        $log_api = new Log_api();
        $log_api->vendor_name  = $vendor_name;
        $log_api->direction    = $direction;
        $log_api->method       = $method;
        $log_api->detail       = $detail;
        $log_api->created_date = date('Y-m-d H:i:s');
        $log_api->web_id       = config('global.web_id');
        $log_api->ip  = getIP();
        
        $log_api->save();

      //   echo "$vendor, $method, $postdata, $direction, $web_id,$status";
      //   exit;
    } // end record_call


    // API-function     
    public function update_api_bal_statement($arr){ 
        $this->update_bal_statement($arr);
    }

    public function get_game_data($vendor_name, $game_id)
    {
        $data = [];
        switch ($vendor_name) {
            case 'TRG':
                $data = $this->trg->game_data($game_id);
                break;
        }
        return $data;
    }

    public function get_egames_list(){
        $vendors = $this->config_vendor_active;
        $vendor_code = [];
        foreach ($vendors as $key => $vendor) {
            $vendor_code[] = $vendor['vendor_id'];
        }
        $platform = 'game_id';
        if(get_channel_id() == 2){
            $platform = 'game_id_mobile';
        }

        $egames_list = Config_lobby_game::where(['game_isactive' => 1, 'game_type' => 'E-Games', 'game_lobby' => 0])
                                            ->whereIn('vendor_id', $vendor_code)
                                            ->whereNotNull($platform)
                                            ->orderBy('vendor_id')
                                            ->orderBy('game_click', 'desc')
                                            ->orderBy('game_isnew', 'desc')
                                            ->get(['game_click','game_type','game_id','game_name','vendor_id', 'game_id_post', 'game_isnew','icon_path','game_isevent']);
        return $egames_list;
    }

    public function hot_game_list(){
        $vendors = $this->config_vendor_active;
        $vendor_code = [];
        foreach ($vendors as $key => $vendor) {
            $vendor_code[] = $vendor['vendor_id'];
        }
        $platform = 'game_id';
        if(get_channel_id() == 2){
            $platform = 'game_id_mobile';
        }

        $hot_game_list = Config_lobby_game::where(['game_isactive' => 1, 'game_lobby' => 0])
            ->where('game_click' ,'>=', 5000)
            ->whereIn('vendor_id', $vendor_code)
            ->whereNotNull($platform)
            ->orderBy('vendor_id')
            ->orderBy('game_click', 'desc')
            ->orderBy('game_isnew', 'desc')
            ->get(['game_click','game_type','game_id','game_name','vendor_id', 'game_id_post', 'game_isnew','icon_path','game_isevent']);

        return $hot_game_list;
    }



    public function authenticate_session($vendor_name,$user,$params=null){
        switch ($vendor_name) {
            case 'VG':
                return $this->vg->authenticate($user,$params);
                break;
            
            default:
                # code...
                break;
        }
    }
            
    
}