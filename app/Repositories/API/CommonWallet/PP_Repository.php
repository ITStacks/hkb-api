<?php

namespace App\Repositories\API\CommonWallet;
use App\Models\DbConfig\{ Config_lobby_game, Adm_config };
use Exception, Log;
use App\Repositories\ApiRepository;

use App\Traits\APITraits;

class PP_Repository extends ApiRepository
{
    use APITraits;
    protected $vendor_name = "PP";
    protected $path = [           
            'AUTH' => "/auth.aspx",                   
            'ENDPOINT' => "/SportsApi.aspx",
    ];
    protected $info = [
        'method'  => '',
        'log_in'  => 1,
        'log_out' => 1,
        'header'  => "",
    ];

    public function __construct()
    {
        try {
            $this->config_vendor_active = parent::$config_vendor_hkb;
            $this->current_config_vendor = $this->config_vendor_active[$this->vendor_name];
            //PP Details
            $this->vendor = $this->current_config_vendor;
            $this->vendorId = $this->vendor['vendor_id'];
            $this->vendorName = $this->vendor['vendor_name'];
            $this->vendorAlias = $this->vendor['vendor_alias'];   
            $this->vendorMaintApi = $this->vendor['maint_api']; 
            $this->vendorOperatorId = $this->vendor['vendor_operator_id'];
            $this->vendorKey = $this->vendor['vendor_key'];
            $this->vendorConversion = $this->vendor['conversion'];
            $this->vendorPrefix = $this->vendor['vendor_prefix'];
            $this->vendorAPIUrl = $this->vendor['vendor_api_url'];
            $this->vendorLobbyUrl = $this->vendor['vendor_lobby_url'];
            $this->vendorMinTransfer = $this->vendor['min_transfer'];
            $this->vendorMaxTransfer = $this->vendor['max_transfer'];            

            $endpoint = "/api/process_request";
            $details = $this->getAPIBridgeLink($endpoint); 
            $this->APILink = $details['url'];
            $this->info['header'] = $details['header'];

            //$this->is_apiBridge =  $this->vendor['is_apibridge'];
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }        
    }

    public function game_url($user, $game_id)
    {        
        try{
            $this->info['method'] = "getgameurl";    

            $game_id_field = 'game_id_mobile';
            if(get_channel_id()==1){
                $game_id_field = 'game_id';
            }

            $check_game = Config_lobby_game::where($game_id_field, $game_id)->first(); 
            if(empty($check_game) && !isWap()) return 0;
            $platform = 'WEB';
            if(get_channel_id()==2){
                $platform = 'MOBILE';
            }
            $technology = 'H5';
            $flash_game_id = ['vpa', 'kna'];
            if(in_array($game_id, $flash_game_id) && get_channel_id() != 2){
                $technology = 'F';
            }
            $sess = (session('user_lastloginsession')) ?? ""; 
            $wl_format_token = Adm_config::where('id', 46)->first();
            $postData = array(
                            'secureLogin' => $this->vendorOperatorId,
                            'symbol' => $game_id, //bjma
                            'language' => 'id',
                            'platform' => $platform,
                            'technology' => $technology,
                            'stylename' => $this->vendorOperatorId,
                            'cashierUrl' => route('deposit'),
                            'lobbyUrl' => route('home')
                        );
            if($sess){
                $postData['token'] = str_replace('_', '', $this->vendorPrefix).$wl_format_token->value.$sess;
            }
            $hash = $this->hash_calc($postData);
            $postData['hash'] = $hash;
            $query_string = urldecode(http_build_query($postData));
            $url = $this->vendorAPIUrl."/http/CasinoGameAPI/game/url";          
            $gameurl = "";
        
            // if($this->is_apiBridge){                
                $postData['vendorName'] = "PP";
                $postData['requestMethod'] = "POST";
                $postData['providerURL'] = $url;
            // } else {                
            //     $this->APILink = $url;
            //     $this->info['header'] = "";
            // }

            $res = $this->getCURL($this->vendorName, $this->APILink, $this->info, $postData);                           
            $json = json_decode($res, true);

            if(isset($json['error']) && $json['error'] == 0){
                $gameurl = $json['gameURL'];
            }else{
                throw new Exception("Wrong JSON check res");
            }
        }catch(Exception $e){
            if(isset($postData)){
                $detail=urldecode(http_build_query($postData));
                $log_api['req'] = $this->APILink.'?'.$detail;
            }
            if(isset($res)){
                $log_api['res'] = $res;
            }
            $log_api['error_message'] = $e->getMessage().' on line '.$e->getLine();
            $log_api['server']=getServerIP();
            $print_error = json_encode($log_api);
            Log::debug("Error PP GameUrl : ".$print_error);
            $this->record_api_call("PP", 1, "fail-getgameurl", $print_error); 
        }    
        return $gameurl;       
    }

    public function hash_calc($value=[])
    {
        if(!$value){
            return;
        }

        ksort($value);
        $string = urldecode(http_build_query($value)).$this->vendorKey;
        $str_md5 = md5($string);
        // dd($string, $str_md5);
        return $str_md5;
    }
}