<?php

namespace App\Repositories\API\CommonWallet;
use App\Models\{Adm_transfer, User_coin};
use App\Models\DbConfig\{ Config_curr };
use DB, Exception, Log;
use App\Repositories\ApiRepository;

class CommonWalletRepository extends ApiRepository
{
	public function __construct()
	{
		$this->config_vendor_active= parent::$config_vendor_hkb;
	}

	public function balance($vendor_name, $user, $echo=0){
		$vendor_id = $this->config_vendor_active[$vendor_name]['vendor_id']; 
        $balance = $this->getUserCoin($vendor_id, $user->user_id, $echo);
        $balance = ($balance == null) ? 0 : $balance;
		if($echo == 1){
            return ['status'=>200, 'balance'=>$balance];
		}else{
			return $balance;
		}
	}

	public function transfer($user, $arr)
    {
    	if($arr['from'] == "ALL" && $arr['type'] == 0){
    		$vendor_name = $arr['vendor_name'];
    	}else{
    		$vendor_name = $arr['to'];
    	}
        $vendor=$this->config_vendor_active[$vendor_name];
    	$vendor_alias = $vendor['vendor_alias'];
        $vendor_id = $vendor['vendor_id'];

        $user_id = $user->user_id;
        $user_name = $user->user_name;

        if($vendor['maint_api'] == 1){
            return -1;
        }
        
        $min_transfer   = $vendor['min_transfer']; //default provider minimum transfer
        // $max_transfer   = $vendor['max_transfer'];
        $point          = $vendor['conversion'];
        $arr["amount"] = $arr["amount"] / $point;

        if($arr['from'] == "ALL" && $arr['type'] == 0){
            $arr["amount"] = $this->getUserCoin($vendor_id, $user->user_id);
            $arr['type'] = 0;
        }

        if(is_array($arr["amount"]))return -2;
        $arr["amount"] = round(trim($arr["amount"]),3); 

        $vendor_balance = $this->getUserCoin($vendor_id, $user->user_id);

        if ($arr['type'] == 1) { // Transfer Out
            $balance = $this->getCurrentUserBalance($user_id);
            $arr["amount"]= $balance / $point;
            if($balance <$min_transfer) return 1;
            $arr["amount"]=  floor(trim($arr["amount"])); 
        } else if($arr['type']==0) { // Transfer In
            if($arr["amount"] > $vendor_balance)return -1;
            if($arr["amount"] < $min_transfer) return 1;
        }else{
            return -1;
        }

        if($arr["amount"] == 0) return 1;

        if($arr["amount"] < 0) return -2;
        $chips = $arr["amount"];
 
        $transfer= $chips * $point;

        $trans_id = Adm_transfer::insertTryCatch([
            "web_id"        => config('global.web_id'),
            'date'          => date("Y-m-d H:i:s"),
            'user_id'       => $user_id,
            'type'          => $arr['type'],
            'amount'        => $chips,
            "after_amount"  => -1,
            'status'        => 0,
            'curr_id'       => $user->curr_id,
            'vendor_id'     => $vendor_id
        ]); 

        if($trans_id==0) return -2; 

        $curr = Config_curr::where('curr_id', $user->curr_id)->first(['code']);
        $ucurr = $curr->code;
 
        if($arr['type']==1){ 
            DB::beginTransaction();
        	try{
	            $current_balance = $balance - $transfer;
	            $arr_update=array();                                         
	            $arr_update['act']=4;
	            $arr_update['user_id']=$user_id;
	            $arr_update['user_name']=$user_name;
	            $arr_update['current_balance']=$current_balance;
	            $arr_update['amount']=$chips * $point;
	            $arr_update['trans_id']=$trans_id;
	            $arr_update['vendor_id']=0;
	            $arr_update['vendor_name']=$vendor_name;
	            
                $this->update_api_bal_statement($arr_update); 
                
                $type = "CR";
                User_coin::updateBalance($user_id,$vendor_id,$type,$chips);

            	$initial_balance = $vendor_balance;
            	$vBalance = $this->getUserCoin($vendor_id, $user->user_id);
				if($vBalance < $initial_balance){
					throw new Exception('failed transfer'); 
				}else{
					$affected=Adm_transfer::where('transfer_id', $trans_id)->update(['status'=>1, 'after_amount'=>$vBalance]);
	                if($affected ==0)throw new Exception('failed update transfer status');  
            	} 
                DB::commit();
	        }catch(\Exception $e){ 
                DB::rollback();
                $detail=array(
                        'user_name'      => $user_name,
                        'type'           => $arr['type'],
                        'amount'         => $chips,
                        "failed_status"  => $e->getMessage(),
                        'status'         => 0,
                        'currency'       => $ucurr,
                        'url'            => (isset($url)?$url:""),
                        'postdata'       => (isset($postdata)?$postdata:""),
                        'response'       => isset($res)?$res:""
                        );
                $detail=json_encode($detail);
                Log::debug("Error CommonWallet Transfer : ".$detail." || message : ".$e->getMessage());
                $this->record_api_call($vendor_name,$arr['type'], "fail-transfer", $detail); 

                return 0; 

	        }

        }elseif($arr['type']==0){ // Transfer In
            DB::beginTransaction();
        	try{
	            $balance= $this->getCurrentUserBalance($user_id); 

	            $current_balance = $balance + $transfer;
	            if($current_balance > $balance){
                    $type = "DB";
                    User_coin::updateBalance($user_id,$vendor_id,$type,$chips);

	            	$arr_update=array();

	            	$arr_update['act']=3;
	            	$arr_update['user_id']=$user_id;
	            	$arr_update['user_name']=$user_name;
	            	$arr_update['current_balance']=$current_balance;
	            	$arr_update['amount']=abs($chips * $point);
	            	$arr_update['trans_id']=$trans_id;
	            	$arr_update['vendor_id']=0;
	            	$arr_update['vendor_name']=$vendor_name;

	            	$this->update_api_bal_statement($arr_update);

    
	            	$affected=Adm_transfer::where('transfer_id', $trans_id)->update(
	            		['status'=>1, 'after_amount'=>$this->getUserCoin($vendor_id, $user->user_id)]
	            	);

	            	if($affected ==0)throw new Exception('failed update status');
	            } 
                DB::commit();
	        }catch(\Exception $e){ 
                DB::rollback();
                $detail=array(
                        'user_name'      => $user_name,
                        'type'           => $arr['type'],
                        'amount'         => abs($chips),
                        "failed_status"  => $e->getMessage(),
                        'status'         => 0,
                        'currency'       => $ucurr,
                        'postdata'       => isset($postdata)?$postdata:"",
                        'response'       => isset($res)?$res:""
                        );
                $detail=json_encode($detail);
                Log::debug("Error CommonWallet Transfer : ".$detail." || message : ".$e->getMessage());
                $this->record_api_call($vendor_name,$arr['type'], "fail-transfer", $detail); 

                return 0; 
	        }
        }
        if($arr['type']==1) $log_id=503;
        else $log_id=504;

        $logs['user_name'] =  $user_name;
        $logs['vendor_name'] =  $vendor_name ;
        $logs['transfer_id'] =  $trans_id ;
        $logs['amount'] = number_format($chips) ; 
        $this->user_log($log_id, $logs);  
            
        //if transfer in/out succeeds
        return $chips;
    }

    public function check_trans($trans_id, object $transfer_details= null)
    {
    	$status = $transfer_details->status;

    	if($status==0){ // status transfer GAGAL
            $update_status=2; // canceled
            if($transfer_details){
                $transfer_type=$transfer_details->type;
                if($transfer_type==1){
                    $update_status=$this->check_refund($transfer_details); 
                }else if($transfer_type==0){                      
                    $update_status=2; 
                }
            }     
            $array_update=array(
                                'status'=>$update_status, 
                                'after_amount'=>0
                            );

            $update=Adm_transfer::where('transfer_id', $trans_id)->update($array_update);   
        }

      	return $status; // 0 => pending , 1 => success
    }
}