<?php
	function identifier($max = 8){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $random_user_id = substr(str_shuffle($chars), 0, $max);
        return $random_user_id;
    }

	function checkFundActivity(){
		if(!session('do_transfer') && !session('do_withdraw')){
			return 0; // no fund activity
		}else{
			return 1; // has fund activity
		}
	} 
	function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        } 
        return $bytes;
	}

	function multiKeyExists($key,array $arr) { 
	    // is in base array?
	    if (array_key_exists($key, $arr)) {
	        return true;
	    }

	    // check arrays contained in this array
	    foreach ($arr as $element) {	 
	    		// if array   	
	        if (is_array($element)) {
	            if (multiKeyExists($key,$element)) {
	                return true;
	            }
	        } 
	    }

	    return false;
	} 

	function clearAccName($value=''){
		$newValue = preg_replace('#[^a-zA-Z0-9\s]#', '', $value);
		return $newValue;
	}

	function clearScript($value=''){
		// function to clear any tag < >
		$newValue = preg_replace('#<(.*)>#', '', $value);
		return $newValue;
	}
	function cloudRefName($name=""){
		$newName = $name;
		if($name){
			$name = substr($name, 1, 2);
			$newName = "XX".trim($name)."XX";
			if ((6 - strlen($newName)) == 1) {
				$newName .= "X";
			}
		}
		return $newName;
	}
	function replaceDocumentVariable($content,$page="ref-general-all"){ 
		$subweb=ucfirst(config('global.subweb_domain'));
		$imagename=ftpUrl();
		if(env('AWS_URL')){
			$imagename .= "public";
		}
		$currenturl=$_SERVER["SERVER_NAME"];
		$dummy = ["domain.com", "https://imagename.com", "currenturl.com"];
		$sitesname = [$subweb, $imagename, $currenturl];

		$final_str = '';
		if($content){
			$str=str_replace($dummy,$sitesname,$content); 

			$dom = new \DOMDocument;
			libxml_use_internal_errors(true);
			$dom->loadHTML($str);
			$dom->strictErrorChecking = false;

			$checks = $dom->getElementById($page);
			$final_str = $dom->saveHTML($checks);
		}

		return $final_str;

	} 
	function compareFloatNumbers($a,$b){
		if (abs(($a-$b)/$b) < 0.00001){
			return 1;
		} 
		return 0;
	}

	function showMaxString($string)
	{ 
		$limit=10;
		if(strlen($string)>10){
			$string=substr($string,0,$limit)."...";
		}
		return $string;
	}

	function paginateCollection($items, $perPage = 15, $page = null)
	{
		$page = $page ?: (\Illuminate\Pagination\Paginator::resolveCurrentPage() ?: 1);
		$items = $items instanceof \Illuminate\Support\Collection ? $items : \Illuminate\Support\Collection::make($items);
		$options = [
            'path' => \Illuminate\Pagination\Paginator::resolveCurrentPath(),
            'pageName' => 'page'
        ];
		return new \Illuminate\Pagination\LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
	}
	function ftpUrl($path=""){
		$private_ftp_domain = (env('AWS_URL', '')) ? env('AWS_URL') : config('global.private_ftp_domain');
		if(config('global.url_scheme') == 'https'){
        	$private_ftp_domain = str_replace('http://', 'https://', $private_ftp_domain);
        }
		$domain_image = ($private_ftp_domain)? : 'https://'.config('global.user_basepath').'.'.config('global.ftp_domain');
		$paths = '/'.$path;
		$paths = str_replace('//', '/', $paths);

		return $domain_image.'/'.$path;
	}
	function backUrlHtml($param = ''){
		$html_url = '<meta name="viewport" content="width=device-width, initial-scale=1">';
		
		switch ($param) {
			case 'home':
				$html_url .= '<a href="'.route('home').'">[ '.trans('home.back.to', ['var1' => trans('home.main', ['var1' => trans('home.menu')])]).' ]</a><br><br>';
				break;
			default:
				$html_url .= '<a href="'.url()->previous().'">[ '.trans('home.back.default').' ]</a><br><br>';
				break;
		}
		return $html_url;
	}
	function json1($x){
		echo "<br>".json_encode($x)."</br>"	;
	}
	function getSiteUrl(){
	  $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ||$_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	  $domainName = $_SERVER['HTTP_HOST'];
	  return $protocol.$domainName;
	}
	function isMobile(){
		$agent = new Jenssegers\Agent\Agent;
		if($agent->isMobile() || $agent->isTablet()){
			return true;
		}else{
			return false;
		}
	}
	function getBro(){
		$agent = new Jenssegers\Agent\Agent;
		return $agent->browser();
	}
	function isWap(){
		if(get_channel_id()==3) {
			return true;
		}
        return false;
	}
	function isApk(){
		if(get_channel_id()==4) {
			return true;
		}
        return false;
	}
	function apkVersion(){
		try {
			return filter_var(explode('/',getBrowser()['name'])[1],FILTER_SANITIZE_NUMBER_INT);			
		} catch (\Throwable $th) {
			return 0;
		}
	}
	function isSafari(){
		$agent = strtolower(getBrowser()["name"]);
		if(getBro()==='Safari' && $agent == "apple safari"){
		 	return true;
	 	}
		return false;
	}
	function isOpera(){
		// if(getBro()==='Opera' ){
		if(getBro()==='Opera' || (preg_match('/OPT/i',$_SERVER['HTTP_USER_AGENT'])) ){
		 	return true;
	 	}
		return false;
	}
	function isCookieMobile(){
		if(Cookie::get('channel')=='mobile') {
			return true;
		}
		return false;
	}
	function convertDate($pattern,$time_value){
      return date($pattern,strtotime($time_value));
	}
	function cloudAccNumber($number){
      return str_pad(substr($number, 3),  strlen($number), "x", STR_PAD_LEFT);
	}
	function separateAccNumber($number){
		$len=strlen($number);
		$remaining=6;
      return  substr($number, 0, 3).'-'.substr($number, 3, 3).'-'.substr($number,$remaining);
	}

	function cloudAccNumberFormat($number){
      return separateAccNumber(str_pad(substr($number, 6),  strlen($number), "x", STR_PAD_LEFT));
	}

	function downloaded_file_name(){
		$uri = Route::getFacadeRoot()->current()->uri();
		$filename = substr($uri, stripos($uri, app()->getLocale()."/")+3).date('Y-m-d');
		return $filename;
	}
	function balance_format($number){
		return number_format((float)$number,2,'.',',');
	}
	function coin_format($number){
		return number_format((float)$number,2,',','.');
	}

	function destroySession(){
		session()->invalidate(); 
		session()->regenerateToken();
	}

	function assetVersion($asset) {
		if (strpos($asset, '.css') !== false){
			return asset(config('global.assetpath') . '/' . $asset.'?v'.config('app.app_version'));
		}else{
			return asset(config('global.assetpath') . '/' . $asset.'?v='.config('app.app_version'));
		}
	}

	function assetAdmin($asset) {
		return asset( 'admin/' . $asset.'?v='.config('app.app_version'));
	}

	function assetglobal($asset) {
		if (strpos($asset, '.css') !== false){
			return asset($asset.'?v'.config('app.app_version'));
		}else{
			return asset($asset.'?v='.config('app.app_version'));
		}
	}

	function curl_post($url, $params = []){
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 20 );
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

		$response = curl_exec($curl);

		if (curl_errno($curl)){
			$response = "";
		}
		
		curl_close($curl);

		return $response;
	} 

	function getBrowser(){ 
	    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
	    $bname = 'Unknown';
	    $platform = 'Unknown';
	    $ub = 'Unknown';
	    $version= "";

	    //First get the platform?
	    if (preg_match('/linux/i', $u_agent)) {
	        $platform = 'linux';
	    }
	    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
	        $platform = 'mac';
	    }
	    elseif (preg_match('/windows|win32/i', $u_agent)) {
	        $platform = 'windows';
	    }
		if($platform=='Unknown'){
			$platform=$u_agent;
		}

	    // Next get the name of the useragent yes seperately and for good reason
		 #special hkb apk
	    if(preg_match('/apps-android/i',$u_agent)) 
	    { 
	    	preg_match('/(\[.*\])/i', $u_agent, $match);
	        if(isset($match[0]))$bname=$match[0];
	        $ub = "android"; 
	    }
	    elseif(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
	    { 
	        $bname = 'Internet Explorer'; 
	        $ub = "MSIE"; 
	    } 
	    elseif(preg_match('/Firefox/i',$u_agent)) 
	    { 
	        $bname = 'Mozilla Firefox'; 
	        $ub = "Firefox"; 
	    } 
	    elseif(preg_match('/Chrome/i',$u_agent)) 
	    { 
	        $bname = 'Google Chrome'; 
	        $ub = "Chrome"; 
	    } 
	    elseif(preg_match('/Safari/i',$u_agent)) 
	    { 
	        $bname = 'Apple Safari'; 
	        $ub = "Safari"; 
	    } 
	    elseif(preg_match('/Opera/i',$u_agent)) 
	    { 
	        $bname = 'Opera'; 
	        $ub = "Opera"; 
	    } 
	    elseif(preg_match('/Netscape/i',$u_agent)) 
	    { 
	        $bname = 'Netscape'; 
	        $ub = "Netscape"; 
	    } 
		if($bname=='Unknown'){
			$bname=$u_agent;
		}

	    // finally get the correct version number
	    $known = array('Version', $ub, 'other');
	    $pattern = '#(?<browser>' . join('|', $known) .
	    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	    if (!preg_match_all($pattern, $u_agent, $matches)) {
	        // we have no matching number just continue
	    }

	    // see how many we have
	    $i = count($matches['browser']);
		if( $ub !='Unknown'){
			if ($i != 1) {
				//we will have two since we are not using 'other' argument yet
				//see if version is before or after the name
				if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
					$version= $matches['version'][0] ?? "";
				}
				else {
					$version= $matches['version'][1] ?? "";
				}
			}
			else {
				$version= $matches['version'][0] ?? "";
			}
		}
	    // check if we have a number
	    if ($version==null || $version=="") {$version="?";}

	    return array(
	        'userAgent' => $u_agent,
	        'name'      => $bname,
	        'version'   => $version,
	        'platform'  => $platform,
	        'pattern'    => $pattern
	    );
	}
	 
	function getServerIP(){

		$lastIP="";
		if(env('APP_ENV')!="local"){
			$lastIP=explode(".",(!isset($_SERVER['SERVER_ADDR']))?$_SERVER['LOCAL_ADDR']:$_SERVER['SERVER_ADDR'])[3];
		}

		return $lastIP;
	}

	function getIP(){
		if(strlen(get_detilIP())>20) {
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else {
			return get_detilIP();
		}
	}

	function get_detilIP() {

	 	// check for IPs passing through proxies
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	   	
	   	// check if multiple ips exist in var
			$iplist = explode(', ', $_SERVER['HTTP_X_FORWARDED_FOR']);
			foreach ($iplist as $ip) {
				if (validate_ip($ip)){
					return $ip;
				}
			}

	  	// check for shared internet/ISP IP
			if (!empty($_SERVER["HTTP_CF_CONNECTING_IP"])){
				return $_SERVER["HTTP_CF_CONNECTING_IP"];
			}

			if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])){
				return $_SERVER['HTTP_CLIENT_IP'];
			}
		}

		if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED'])){
			return $_SERVER['HTTP_X_FORWARDED'];
		}
		if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])){
			return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
		}
		if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR'])){
			return $_SERVER['HTTP_FORWARDED_FOR'];
		}
		if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED'])){
			return $_SERVER['HTTP_FORWARDED'];
		}

	  	// return unreliable ip since all else failed
		return $_SERVER['REMOTE_ADDR'];
	}

	function validate_ip($ip) {
		return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE);
	}

	function get_channel_id(){
		$channel = 1;
		if(config('global.channel') == 'm'){
		    $channel = 2;
		    $userAgent=getBrowser();
		    if(stripos($userAgent['name'],'android')!== FALSE) {
		    	$channel=4;
		    }
		}elseif (config('global.channel') == 'wap') {
		    $channel = 3;
		}

		return $channel;
	}

	function vendor_option_reader($value){
		$data = [];
		$arrValue = explode(';', $value); // separator between items
		foreach ($arrValue as $i => $properties) {
			$prop = explode('#', trim($properties));// separator between name and value
			$data[$prop[0]] = $prop[1];
		}
		return $data;
	}
	
	function adm_config_reader($value, $view=0){
		$data = [];
		$arrValue = explode(',', $value);
		foreach ($arrValue as $i => $properties) {
			$prop = explode(':', $properties);
			if(stripos($prop[1], '#') !== false){
				if($view){
					continue;
				}else{
					$prop[1] = substr($prop[1], 1);
				}
			}
			$data[$prop[0]] = capitalize_space($prop[1]);
		}
		return $data;
	}

	function capitalize_space($text){
		$arr = str_split($text);
		$newArr = array();
		for($i=0;$i<count($arr);$i++){
			if(preg_match("#[A-Z]#",$arr[$i]) && $i != 0){
				$newArr[] = " ".$arr[$i];
			}else{
				$newArr[] = $arr[$i];
			}
			
		}
		return implode("",$newArr);
	}

	function getQuartalPartition($date){
		$monthnumber=date('n',strtotime($date));
		$quarter=floor(($monthnumber - 1) / 3) + 1;
		$q="q".$quarter;
		$year=date('Y',strtotime($date));
		$partition="p".$year.$q;

  		return $partition;
	}

	function getQuartal($date){
		$monthnumber=date('n',strtotime($date));
		$quarter=floor(($monthnumber - 1) / 3) + 1;
		return $quarter;
	}
 	
	function getQuartalName($year,$number){
		$q="q".$number; 
		$partition="p".$year.$q; 
  		return $partition;
	}

	// range 1tahun
	// cth 1
	// kalau beda tahun maka
	// q1= 1 october 2018,1 dec 2018 (p2018q4)
	// q2= 1 january 2019, 1 februari 2019(p2019q1)

	// cth 2
	// q1= 1 agustus 2018,1 dec 2018 (p2018q3),(p2018q4)
	// q2= 1 january 2019, 1 februari 2019(p2019q1)


	function getQuartalPartitionRange($start_date,$end_date){

		if( strtotime($end_date)<strtotime($start_date)){
            $end_date=$start_date;
        }

		$start_year=date('Y',strtotime($start_date));
		$end_year=date('Y',strtotime($end_date));

	 	$start_partition=getQuartal($start_date);
	 	$end_partition=getQuartal($end_date);
	 	$start_partition1=$start_partition+1;

	 	$range=$end_partition-$start_partition;
	 	$range1=$range+1;

  		$range_partition=getQuartalName($start_year,$start_partition);
	 	
	 	if(($start_year<=2018 && $end_year<=2018) || $start_year<=2018){
	 		$range_partition=getQuartalName(2018,4);
	 		if($end_year>2018){
		 		for($i=1;$i<=$end_partition;$i++){ 		 	
				 	$range_partition.= ','.getQuartalName($end_year,$i);
				}
		 	}
	 		return $range_partition;
	 	}
	 	
	 	
  		if($start_year!==$end_year){
		 	for($i=$start_partition1;$i<=4;$i++){ 		 	
			 	$range_partition.= ','.getQuartalName($start_year,$i);
			}
			for($i=1;$i<=$end_partition;$i++){ 		 	
			 	$range_partition.= ','.getQuartalName($end_year,$i);
			}
	 	}else{
	 		if($range==0) {
	 			return  getQuartalName($end_year,$end_partition);
	 		}
		 	for($i=1;$i<$range1;$i++){ 		 	
			 	$range_partition.= ','.getQuartalName($end_year,$start_partition1);
			 	$start_partition1++;
		 	}
	 	}
		 return $range_partition;
	}

	function getYearPartition($date){
		$monthnumber=date('n',strtotime($date));
		$quarter=floor(($monthnumber - 1) / 3) + 1;
		$q="q".$quarter;
		$year=date('Y',strtotime($date));
		$yearPartition="p".$year;

  		return $yearPartition;
	}

    function getMonthlyPartitionRange($start_date, $end_date){
        // Check if Start date > End date
        if(strtotime($end_date) < strtotime($start_date)){
            $end_date = $start_date;
        }

        $start_year = date("Y", strtotime($start_date));
        $start_month = date("m", strtotime($start_date));

        $end_year = date("Y", strtotime($end_date));
        $end_month = date("m", strtotime($end_date));

        $range_partition = "";

        for($start_year; $start_year <= $end_year; $start_year++){
            // Set end month to last if still have next
            if($start_year != $end_year) {
                $end_month = 12;
            }else {
                $end_month = date("m", strtotime($end_date));
            }

            for($start_month; $start_month <= $end_month; $start_month++) {
                $range_partition .= ",p" . $start_year . sprintf("%02d", $start_month);
                if($start_month == 12) {
                    $start_month = 1;
                    break;
                }
            }
        }

		$range_partition = ltrim($range_partition, ',');
        return $range_partition;
    }
