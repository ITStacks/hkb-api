<?php

namespace App\Traits; 

use App\Models\DbConfig\{Adm_config, Vendors };
use Illuminate\Support\Facades\Http;
use Session;
trait APITraits
{ 

   public function getAPIBridgeLink($value){
      $detail = array();
      if(!Session::has('api_link_domain')){
         $api_link_domain = Adm_config::getAdmConfigValue(62);
         session(['api_link_domain' => $api_link_domain]);
      } else {
         $api_link_domain = session('api_link_domain');
      }
      $api_link_endpoint = $value;		

      $url = $api_link_domain . $api_link_endpoint;	
      $details['url'] = $url;

      if(!Session::has('api_bridge_token')){
         $api_bridge_token = Adm_config::getAdmConfigValue(64);
         session(['api_bridge_token' => $api_bridge_token]);
      } else {
         $api_bridge_token = session('api_bridge_token');
      }
      $details['header'] = array("Accept" => "application/json",	
                                 "Content-type" => "application/json",					   
                                 "Authorization" => "Bearer ". $api_bridge_token);

      if($value == "/api/process_request"){ //Value format need when using apirepo getCurl
         $details['header'] = ["Accept: application/json",	
                               "Content-type: application/json",					   
                               "Authorization: Bearer ". $api_bridge_token];
      }
     
      return $details;						   
  }

  /*public function getAPIBridgeResponse($api_link,$headers,$vendor_name,$vendor_prefix){           
      $item = array();        
      $id = $vendor_name . "-" . str_replace('_','',$vendor_prefix);
      $url = $api_link.'/'.$id.'/edit';	      
      $json = Http::withHeaders($headers)->get($url)->json();
   
      $item['api_link_id'] = -1;
      $item['api_link'] = NULL;

      if(isset($json['error_code'])){ 
 
         if($json['error_code'] == 0){
            if(!is_null($json['data'])){
              $item['api_link_id'] = $json['data']['id'];
              $item['api_link'] = $json['data']['api_link'];
            } else {
              $item['api_link_id'] = 0; 
            }
         }elseif($json['error_code'] == 1){
            $item['api_link_id'] = -1;
         }
      }
         
      return $item;
   }*/
}