<?php 
namespace App\Traits; // *** Adjust this to match your model namespace! ***

    use Illuminate\Database\Eloquent\Builder;
    use App\Models\{Join_balance,Join_lastorder, Log_admin};
    use App\Models\User;
    use App\Models\User_coin;
    use App\Models\{Log_user, Trans,Trans_raw, Trans_promo,Join_bonus,Adm_transaction_day};
    use App\Models\{User_notify,Message};
    use App\Models\{Adm_transfer,Forgot_pass};
    use App\Models\DbConfig\{ Config_livechat, Partner_web, Config_bank };

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Config;
   	use Cookie,Session,Exception,Log;
   	use App\Repositories\ApiRepository;

trait UserTraits { 

    public function getUrlApiLogin($request, $user, $session)
    {
        $key = date('Y-m') . env('APP_KEY') . "hkb88";
        $userdata["user"] = $user->user_name;
        $pass = $user->user_pass;

        $userdata["hash"] = hash('sha256', $userdata["user"] . $pass . $session . $key); 

        if(config('global.channel')){
            $userdata["channel"] = config('global.channel');
        }
        
        $query_string = http_build_query($userdata); 
        $url = $request->domain . '/api_login?' . $query_string; 
        
        return ['status' => 202, 'message' => 'Success', 'next_url' => $url];
    }

    public function checkUserCoinLock($user_id){
        // check user coin lock in 5 seconds
        return User_coin::onWriteConnection()
        ->withoutGlobalScopes()
        ->where('user_id',$user_id)
        ->where('web_id',config('global.web_id'))
        ->where('vendor_id',0)
        ->where(function ($query) {
            $query->where('is_locked',1) 
                ->orWhere([
                        ['last_locked_at', '<=', date('Y-m-d H:i:s', time()-5)],
                        ['last_locked_at', '>=', date('Y-m-d H:i:s', time()-10)]
                            ]);
        })->count();
    }
    

    public function setUserCoinLock($user_id){
        return User_coin::onWriteConnection()
        ->withoutGlobalScopes()
        ->where('user_id',$user_id)
        ->where('web_id',config('global.web_id'))
        ->where('vendor_id',0)
        ->where(function ($query) {
            $query->where('is_locked',0)
                ->orWhereNull('last_locked_at')
                ->orWhere('last_locked_at', '<=', date('Y-m-d H:i:s', time()-5*60));
        })
        ->update(['is_locked'=>1,'last_locked_at' => date('Y-m-d H:i:s')]);
    }

    public function unsetUserCoinLock($user_id){
        return User_coin::onWriteConnection()
        ->withoutGlobalScopes()
        ->where('is_locked',1)
        ->where('user_id',$user_id)
        ->where('web_id',config('global.web_id'))
        ->where('vendor_id',0)
        ->update(['is_locked'=>0]);
    }

    public function addVariableAfterLogin($user){ 

        Forgot_pass::where(['isdone' => 0, 'type' => 'LGN', 'user_id' => $user->user_id ])->delete();

        $log_id=514; 
        $logs['desc'] = $this->getBrowserDetails(); 
        $logs['v'] = config('app.app_version');
        $logs['server'] = getServerIP();
        $this->user_log($log_id, $logs);  

        $sessions = [ 
            "user_lastloginsession"     =>  $user->last_session, 
            "firstLogin"               =>  1
        ];
        session($sessions);

    }

    public function getBrowserDetails(){

      $browser = getBrowser();

      if(config('global.channel')=="m") {
        $channel = "mobile";  
        if(isApk())$channel='apk';
      }elseif (config('global.channel')=="web"){
        $channel = "desktop";
      }else {
        $channel = "wap";
      }

      $data= htmlspecialchars($_SERVER['SERVER_NAME'].' | '.$browser['name'] . " " . $browser['version'] . " on " .$browser['platform'] ." | ".$channel);  

      if($channel=="apk") $data= htmlspecialchars($browser['name'] . " " . $browser['version'] . " on " .$browser['platform'] ." | ".$channel);  

      return $data; 
    }

    public function check_balance($code,$alert){

        $this->api = new ApiRepository;
        
        $res = $this->api->check_balance($code,$alert);

        if(strtolower($code)!='balance'){ 
            if( isset($res['balance'])){
                $last_balance=$res['balance']; 
                $vendor=$this->api->getActiveVendorList();   
                if(!isset($vendor[$code])){
                    return 0;
                }
                $vendor = $vendor[$code];

                if($vendor['wallet_type'] == "TW"){ 
                    User_coin::updateOrCreate(
                            [ 
                            "web_id"   => config('global.web_id'),
                            'user_id'  =>  Auth::user()->user_id, 
                            'vendor_id'=> $vendor['vendor_id']
                            ],
                            [
                            'coin'=>$last_balance,
                            'val_key'=>DB::raw('SHA2(CONCAT(`user_id`,vendor_id, `coin`), 256)')
                            ]
                        );
                }
                if($alert){
                    $res['balance'] = number_format($res['balance']);
                }
            }
        }

        return $res;
    }

   
    public function checkUserTransferLog($user_name){ 
        
        $delay_seconds = 4; 
        $delayed_time = time()-$delay_seconds;
        $delayed_time = date('Y-m-d H:i:s',$delayed_time);

        $search[] = ['log_date' , '>=' , $delayed_time]; 
        $search[] = ['user_name',$user_name];

        $selectClause = array(
            "log_date",
            "log_id"
                );  

        $quarter=getQuartalPartition($delayed_time);
        $log = Log_user::from(DB::raw('log_user PARTITION('.$quarter.')'))
                    ->where($search) 
                    ->orderBy("id","DESC")  
                    ->first($selectClause); 

        $transfer_log=array(503,504,530);//,508,509,526,527);   

        if(in_array($log['log_id'],$transfer_log))return 1; 
        return 0;
        
    }

    public function checkUserLog($user_name,$log_id){ 
        
        $delay_seconds = 30; 
        $delayed_time = time()-$delay_seconds;
        $delayed_time = date('Y-m-d H:i:s',$delayed_time);

        $search[] = ['log_date' , '>=' , $delayed_time]; 
        $search[] = ['user_name',$user_name];
        $search[] = ['log_id',$log_id];

        $selectClause = array(
            "id"
                );  

        $quarter=getQuartalPartition($delayed_time);
        $log = Log_user::from(DB::raw('log_user PARTITION('.$quarter.')'))
                    ->where($search) 
                    ->orderBy("id","DESC")  
                    ->first($selectClause); 

        if(!empty($log))return 1; 
        return 0;
        
    }

    public function checkPendingTransfer($check_only=0){ 
 
        $db = config('database.connections.db_config.database').'.';

        $user_id=Auth::user()->user_id;
        $user_name=Auth::user()->user_name;
        // $delay=$this->checkUserTransferLog($user_name);
        // if($delay==1) return -1;

       $row = Adm_transfer::onWriteConnection()->join($db.'config_vendor as cv', function ($join){
                                $join->on('cv.vendor_id', '=', 'adm_transfer.vendor_id');
                                $join->on('cv.web_id', DB::raw(config('global.web_id')));
                            })
                           ->where([['user_id','=', $user_id],['status','<',1]])
                           ->first(['status','date','transfer_id','adm_transfer.vendor_id','type','amount', 'cv.vendor_name','cv.conversion']);
        
        if(!empty($row)){
            if($row->status==0){
                $lock = $this->setUserCoinLock(Auth::user()->user_id);
                if(!$lock){
                    return -1;
                }
                $transfer_id=$row->transfer_id;
                $vendor_id=$row->vendor_id;
                if($check_only==1){
                    $this->unsetUserCoinLock(Auth::user()->user_id);
                    return -1;
                }
                
                $this->api = new ApiRepository;
                $status= $this->api->check_transfer($vendor_id,$transfer_id,$row);
                $this->unsetUserCoinLock(Auth::user()->user_id);
                return $status;
                
            }else{
                return -1;
            }
        }

        return 0;
    }

    public function status_user_message(){
        $text = "";
        if(Auth::check()){
            if(Auth::user()->status == 0){
                $text = trans('lang.user_status_0').' '.trans('lang.user_status_mess_1');
            }else if(Auth::user()->status == 2){
                $text = trans('lang.user_status_2').' '.trans('lang.user_status_mess_2');
            }else if(Auth::user()->status == 3){
                $text = trans('lang.user_status_3').' '.trans('lang.user_status_mess_1');
            }else if(Auth::user()->status == 4){
                $text = trans('lang.user_status_4').' '.trans('lang.user_status_mess_1');
            }
        }
        return $text;
    }

    // API-function copy from UserTraits
    public function check_main_balance($user_id,$user_totaldpx=NULL){ 
        
        $check = Join_lastorder::onWriteConnection()
                ->where('user_id', $user_id)
                ->orderBy('join_id','DESC')
                ->first(['balance']);   

        $coin_balance = User_coin::getBalance($user_id,0); 
        if($coin_balance < 0 ) {
            return -1;
        } 

        if($check){ 
            if(round($check["balance"]) != round($coin_balance)){
                if(!isset($user_totaldpx)){
                    $user = User::where('user_id',$user_id)->first(['user_totaldpx']);
                    $user_totaldpx = $user->user_totaldpx;
                } 
                
                if($user_totaldpx == 0) {
                    return 0;
                }else{
                    return -1;
                }           
            }
        }else{
            $coin_balance = 0;
        }
        
        return $coin_balance; 

    }

    public function update_bal_statement($arr){
            $date=date('Y-m-d');
            $descr="";
            $web_id=config('global.web_id');

            $act=$arr['act'];
            $user_id=$arr['user_id'];
            $user_name=$arr['user_name'];
            $before_balance=$arr['current_balance'];
            $amount=$arr['amount'];
            $vendor_id=$arr['vendor_id'];
            $trans_id=$arr['trans_id']; 
            
            if(isset($arr['vendor_name']))$descr=$arr['vendor_name']; 
            if(isset($arr['descr']))$descr=$arr['descr'];

            //1:Deposit,2:Withdraw,3:TransferIn,4:TransferOut,5:CorrectionIn,6:CorrectionOut,7:WithdrawRejected,
            //10:BonusDeposit,11:BonusReferral,12:BonusCommission,13:BonusPromoCode
            $cash_out=$cash_in=0;
            $correction_out=$correction_in=0;
            $transfer_out=$transfer_in=0;
            $bonus=0;
            switch ($act){
                CASE 1: $type = 'CR'; $cash_in=$amount; break;
                CASE 3: $type = 'CR'; $transfer_in=$amount;break;
                CASE 5: $type = 'CR'; $correction_in=$amount;break;
                CASE 7: $type = 'CR'; $cash_in=$amount;break; 
                CASE 10:
                CASE 11:
                CASE 12:
                CASE 13: $type = 'CR';$bonus=$amount;break;
                
                CASE 4: $type = 'DB'; $transfer_out=$amount;break;
                CASE 2: $type = 'DB'; $cash_out=$amount;  break;
                CASE 6: $type = 'DB'; $correction_out=$amount;break; 
                default:exit('error mutasi');
            }
          
                User_coin::updateBalance($user_id,$vendor_id,$type,$amount);    
                $current_balance = User_coin::getBalance($user_id,0);
                // insert into money history
                $quarter=getQuartalPartitionRange($date,$date);
                $r=Adm_transaction_day::from(DB::raw('adm_transaction_day PARTITION('.$quarter.')'))
                                    ->where([
                                  // ['web_id','=',$web_id],
                                  ['user_id','=',$user_id],
                                  ['created_date','=',$date]
                                  ])->first(['id']);

                if(!empty($r)){
                      $arr_update = array(
                                          'bonus'  => DB::raw('bonus + '.$bonus),
                                          'cash_in'  => DB::raw('cash_in + '.$cash_in),
                                          'cash_out'  => DB::raw('cash_out + '.$cash_out),
                                          'correction_in'  => DB::raw('correction_in + '.$correction_in),
                                          'correction_out'  => DB::raw('correction_out + '.$correction_out),
                                          'transfer_in'  => DB::raw('transfer_in + '.$transfer_in),
                                          'transfer_out'  => DB::raw('transfer_out + '.$transfer_out) 
                                       );
                      Adm_transaction_day::from(DB::raw('adm_transaction_day PARTITION('.$quarter.')'))
                        ->where('id', $r->id)
                        ->update($arr_update);

                }else{
                       Adm_transaction_day::insert([
                            'web_id'         => $web_id,
                            'curr_id'        => 101,
                            'user_id'        => $user_id,
                            'bonus'          => $bonus,    
                            'cash_in'        => $cash_in,
                            'cash_out'       => $cash_out,
                            'correction_in'  => $correction_in,
                            'correction_out' => $correction_out,
                            'transfer_in'    => $transfer_in,
                            'transfer_out'   => $transfer_out,
                            'created_date'   => $date   
                          ]);
                }
                //  Insert into join balance utk target user
                $insertClause = array(
                                    'web_id'          => $web_id,
                                    'user_id'         => $user_id,
                                    'user_name'       => $user_name,
                                    'amount'          => $amount,
                                    'type'            => $type,
                                    'balance'         => $current_balance,
                                    'act'             => $act,
                                    'datetime'        => config('created_updated_at'),
                                    'trans_id'        => $trans_id,
                                    'descr'           => $descr
                                );
                
                $lastid=Join_balance::insert($insertClause);
                    //  Insert into joinlastorder utk target user
                $insertClause = array(
                                    'web_id'          => $web_id,
                                    'user_id'         => $user_id,
                                    'amount'          => $amount,
                                    'balance'         => $current_balance,
                                    'act'             => $act,
                                    'datetime'        => config('created_updated_at'),
                                );
                
                $lastid=Join_lastorder::insert($insertClause); 
    }

    public function validate_withdraw_status($user, $amount){
        // return validate status;
        $user_totaldp = $user->user_totaldp;    
        $user_id = $user->user_id;

        $zero_dp_status= (bool)($user_totaldp == 0);
        if($zero_dp_status) return 0;

        $last_deposit = Trans::typeDeposit()->approved()->where('user_id', $user_id)->orderBy('trans_id','desc')->first(['trans_amount','trans_processeddate']);

        if(empty($last_deposit))return 0;
          //sum wd compared to last deposit 
        $accumulated_withdraw=Trans::typeWithdraw()->approved()->where('user_id', $user_id)->where('trans_processeddate', '>=', $last_deposit->trans_processeddate)->sum('trans_amount');
        $total_withdraw=$accumulated_withdraw+$amount;
        $wd_morethan_dp_status= (bool)($total_withdraw > $last_deposit->trans_amount ); 
        
        if($wd_morethan_dp_status)return 2;

        return 1;
    }
 
    public function process_withdraw($user,$amount,$forgot_password=0){
            $lock = $this->setUserCoinLock($user->user_id);
            if(!$lock){
                return -1;
            }
            $validated_status=$this->validate_withdraw_status($user, $amount); 

            // echo $validated_status;
            // exit;

            $amount = floor(trim($amount));
            $user_id        = $user->user_id;
            $accgroup_id    = $user->accgroup_id;
            $user_name      = $user->user_name;
            $bank_id        = $user->user_bank;
            $bank_accnumber = $user->user_acc_number;
            $total_dp = $user->user_totaldp;
            $wd_group = $this->custom->get_user_withdraw_group($accgroup_id);

            $array_trans=array(
                            "web_id"                    => config('global.web_id'),
                            "trans_act"                 => 2, //Withdraw
                            "user_id"                   => $user_id,
                            "bank_id"                   => $bank_id,
                            "acc_number"                => $bank_accnumber,
                            "trans_amount"              => $amount,
                            "wdgroup_id"                => $wd_group,
                            "accgroup_id"               => $accgroup_id,
                            "trans_isvalidated"         => $validated_status,
                            "trans_date"                => date("Y-m-d H:i:s")
                        );

            if($forgot_password==1) $array_trans['descr']= 'forgot password';
            
            try{

                DB::beginTransaction(); 
                $check_last_deposit = Trans::typeDeposit()->approved()->where('user_id', $user_id)->orderBy('trans_id', 'desc')->first(['trans_amount', 'trans_processeddate']);
                $last_deposit = $total_withdraw = 0;
                if(!empty($check_last_deposit)){
                    $last_deposit = $check_last_deposit->trans_amount;
                    $total_withdraw = Trans::typeWithdraw()->approved()->where('user_id', $user_id)->where('trans_processeddate', '>=', $check_last_deposit->trans_processeddate)->sum('trans_amount');    
                }
                $total_withdraw += $amount;

                $wdcolour_id = 1; 
                $wd_morethan_dp_status= (bool)($total_withdraw > $last_deposit );               
                if($wd_morethan_dp_status) $wdcolour_id = 2;
                $zero_dp_status= (bool)($total_dp == 0);
                if($zero_dp_status)$wdcolour_id = 3; 
                $array_trans['wdcolour_id'] = $wdcolour_id;

                 $transId = Trans_raw::firstOrCreate([
                "trans_act"                 => 2, //Withdraw
                "user_id"                   => $user_id,
                ],$array_trans);

                if($transId->wasRecentlyCreated === FALSE) throw new Exception(trans('lang.wd_previous'));    
                        
                if(!isset($transId->toArray()['trans_id'])) throw new Exception(trans('lang.wd_previous')); 

                $transId = $transId->toArray()['trans_id'];

                $balance = User_coin::getBalance($user_id,0,1);
                if($balance < 0) throw new Exception('Error balance problem');  

                $current_balance = $balance - $amount;
                if($current_balance < $balance){  
                    $arr=array();
                                                
                    $arr['act']=2;
                    $arr['user_id']=$user_id;
                    $arr['user_name']=$user_name;
                    $arr['current_balance']=$current_balance;
                    $arr['amount']=$amount;
                    $arr['trans_id']=$transId;
                    $arr['vendor_id']=0;

                    $this->update_bal_statement($arr);

                    User::where('user_id', $user_id)->update([
                        'user_totalwd' => DB::raw('`user_totalwd` + ' . $amount), 
                        'user_totalwdx'=> DB::raw('`user_totalwdx` + 1')
                    ]); 

                    Trans_promo::where('user_id', $user_id)->notDone()->notCanceled()->update(['trans_iscanceled'=>1]);
                    Join_bonus::where('user_id', $user_id)->delete();

    
                    $log_id=502;
                    $logs['user_name'] =  $user_name;
                    $logs['amount']=$amount;
                    $logs['trans_id']=$transId; 
                    $this->user_log($log_id, $logs);  
                }                                

                DB::commit();
                $this->unsetUserCoinLock($user->user_id);
                return 1;
            }catch (\Exception $e){
                
                $this->loggedToFile(["error" => $e->getMessage(),"request" => $array_trans]);

                DB::rollback();
                $this->unsetUserCoinLock($user->user_id);
                return 0;
            }
    }

    public static function admin_log($log_id, $desc=""){

        if($desc!="")$desc=json_encode($desc);
        
        $channel = get_channel_id();
        Log_admin::insert([
            "web_id"   		=> config('global.web_id'),
            'operator_name' => 'AUTO',
            'log_id'        => $log_id,
            'log_desc'      => $desc,
            'log_date'      => date('Y:m:d H:i:s'),
            'log_ip'        => getIP(),    
            'channel'       => $channel   
        ]);
    }

    public static function user_log($log_id, $desc=""){

        if(Auth::check())$user_name=Auth::user()->user_name;
        else $user_name=$desc['user_name'];

        if($desc!="")$desc=json_encode($desc);

        $channel = get_channel_id();
        Log_user::insert([
            'web_id'        => config('global.web_id'),
            'user_name'     => $user_name,
            'log_id'        => $log_id,
            'log_desc'      => $desc,
            'log_date'      => date('Y:m:d H:i:s'),
            'log_ip'        => getIP(),    
            'channel'       => $channel   
        ]);
    }

    //******************* USER NOTIFY

    private function checkNotify(){

        try {
            if(session('check_notify') && time() - session('check_notify_time') <= 5*60){
                return 0;
            }
            $message="";
            $user_id = Auth::user()->user_id;
            $lock = $this->setUserCoinLock(Auth::user()->user_id);
            if(!$lock){
                return 0;
            }
            $set_session = ['check_notify' => 1, 'check_notify_time' => time()];
            session($set_session);
            DB::beginTransaction();
            $notify = User_notify::onWriteConnection()->where('user_id', $user_id)->first(['id', 'notify_id', 'mes','curr_id']); 
            if(empty($notify)) {
                DB::rollback();
                $this->unsetUserCoinLock(Auth::user()->user_id);
                session(['check_notify' => 0]);
                return 0;
            }

     
            $id = $notify["id"];
            $notify_id = $notify["notify_id"];
            $curr_id = $notify["curr_id"];
            $arr = explode(",", $notify["mes"]);
            $amount = (isset($arr[2])) ? round($arr[2],2) : "" ;
            $link = route('memo.read', [trans('lang.inbox'),$notify["mes"]]);
            $inbox = $this->messageCount();
                        
            $currency="";
            if($curr_id==101)$currency="IDR";
            //// Message perlu di perbaiki

            //USER NOTIFY
            //1:MemoWelcome,2:ValidationAccept,3:ValidationReject,
            //4:DepoAccept,5:DepoReject,6:WithdrawAccept,7:WithdrawReject,
            //8:Memo,9:BankChanged
            //,10:BonusDeposit,11:BonusReferral,12:BonusCommission  
            //,13:BonusPromoCode,14:TransferOutAPI
            //ACT
            //{"1":"Deposit","2":"Withdraw","3":"Transfer In","4":"Transfer Out","5":"Correction In","6":"Correction Out","7":"Withdraw Rejected","10":"Bonus Deposit","11":"Bonus Referral","12":"Bonus Commission","13":"Bonus Promo Code"}
            $notify_id_add_balance=[4,7,10,11,12];
            if (in_array($notify_id, $notify_id_add_balance)) {
                $before_balance = $this->check_main_balance( Auth::user()->user_id , Auth::user()->user_totaldpx); 
                if($before_balance<0){
                    $message = trans('lang.trf_error_balance');
                    session(['check_notify' => 0]);
                    return $message;
                }
            }

            $affected=User_notify::where('id' , $id)->delete();
            if(empty($affected)) {
                DB::rollback();
                $this->unsetUserCoinLock(Auth::user()->user_id);
                session(['check_notify' => 0]);
                return 0;
            }
            switch($notify_id){
                CASE 1:  
                        $message=trans('lang.memo_welcome', ['servername'=>str_replace("www.","",$_SERVER["SERVER_NAME"]), 'link'=>$link]);
                        break;
                CASE 2:  
                        Cookie::queue("_val", 1, 5);
                        $this->api = new ApiRepository;
                        $this->api->register_user_to_provider(); 
                        $message=trans('lang.memo_validation_success', ['link'=>$link]);
                        break;
                CASE 3:  
                        $message= trans('lang.memo_validation_fail', ['link'=>$link]);
                        break;
                CASE 4: 
                        $act_id =1; # see line 85
                        $this->addBalance($amount,$act_id,$id);
                        $amount_txt= number_format($amount);
                        $message=trans('lang.memo_transaction_success', ['transaction'=>"Deposit $currency $amount_txt", 'link'=>route('memo')]); 
                        if(config('global.partner_type')==2)$message=trans('lang.memo_transaction_success_agency', ['transaction'=>"Deposit $currency $amount_txt"]);    

                        $log_id=526;
                        $logs['amount'] =  number_format($amount);
                        $logs['notify_id'] =  number_format($id);
                        $this->user_log($log_id,$logs);         
                        break;
                CASE 5:  
                        $message= trans('lang.memo_transaction_fail', ['transaction'=>'Deposit', 'link'=>$link]);
                        break;
                CASE 6:  
                        $message=trans('lang.memo_transaction_success', ['transaction'=>'Withdraw', 'link'=>$link]);
                        if(config('global.partner_type')==2)$message=trans('lang.memo_transaction_success_agency', ['transaction'=>"Withdraw $currency"]);
                        break;
                CASE 7:   
                        $act_id=7; 
                        $this->addBalance($amount,$act_id,$id);
                        $amount_txt= number_format($amount); 
                        $log_id=527;
                        $logs['amount'] =  number_format($amount);
                        $logs['notify_id'] =  number_format($id);
                        $this->user_log($log_id,$logs);         
                        $message=trans('lang.memo_transaction_fail', ['transaction'=>"Withdraw $currency $amount_txt", 'link'=>route('memo')]);
                        break;
                CASE 8:  
                        $message=trans('lang.memo_new_read', ['link'=>$link, 'inbox'=>$inbox]);
                        break;
                CASE 9:  
                        $message=trans('lang.memo_bank_changed', ['link'=>route('deposit')]);
                        User_notify::where('user_id' , $user_id)->where('notify_id', 9)->delete();
                        break;
                CASE 10:
                        $act_id=10;
                        $this->addBalance($amount,$act_id,$id);
                        $message=trans('lang.memo_bonus_deposit', ['amount'=>number_format($amount), 'balance'=>balance_format($before_balance), 'server'=>config('global.webname')]);
                        break;
                CASE 11:
                CASE 12:   
                        $act_id =$notify_id;  
                        $this->addBalance($amount,$notify_id,$id);

                        if($notify_id==11){
                            $log_id=509;
                            $message=trans('lang.memo_bonus_referral', ['amount'=>number_format($amount), 'balance'=>number_format($before_balance) ]);
                        }else{
                            $log_id=508;
                            $message=trans('lang.memo_bonus_cashback', ['amount'=>number_format($amount), 'balance'=>number_format($before_balance), 'server'=>config('global.webname') ]);
                        }  
                        $logs['amount'] =  number_format($amount);
                        $logs['notify_id'] =  number_format($id);
                        $this->user_log($log_id,$logs);  

                        break;
                CASE 13:
                        $act_id=13;
                        $message=trans('lang.memo_bonus_promo', ['amount'=>number_format($amount), 'server'=>config('global.webname')]);
                        break;
                CASE 14:   
                        $act_id=14;  
                        // $vendor = (isset($arr[0])) ? ($arr[0]) : "" ;
                        $vendor = trans('home.game', ['var1' => trans('home.lobby', ['var1' => ''])]);
                        $amount_txt= number_format($amount); 
                        $message=trans('lang.trf_success_amount', ['vendor'=>"$vendor", 'amount'=>$amount_txt]);
                        break;
                default:
                        DB::rollback();
                        $this->unsetUserCoinLock(Auth::user()->user_id);
                        session(['check_notify' => 0]);
                        return 0;
            }      

            DB::commit();    
        
        }catch (Exception $ex) {
            DB::rollBack(); 
            Log::debug("Error process notify_id");
            Log::debug($ex);
        }
        $this->unsetUserCoinLock(Auth::user()->user_id);
        session(['check_notify' => 0]);
        return $message; 
    }

    private function addBalance($amount,$act_id,$id){ 
        // $this->checkPendingTransfer();
        
        $before_balance = Auth::user()->mainBalance();
        $current_balance = $before_balance + $amount;           

        if( $before_balance < $current_balance){   
            
            $arr=array();
                                     
            $arr['act']=$act_id;
            $arr['user_id']=Auth::user()->user_id;
            $arr['user_name']=Auth::user()->user_name;
            $arr['current_balance']=$current_balance;
            $arr['amount']=$amount;
            $arr['trans_id']=$id;
            $arr['vendor_id']=0;

            $this->update_bal_statement($arr); 
        }else
            throw New Exception("Balance is credited");

    } 
    

    public function messageCount(){
        $totalMessage = Message::userAuth()->notDeleted()
                        ->where('message_folder','i')                        
                        ->unread()
                        ->count('message_id');
        return $totalMessage;
    }

    public function get_livechat($global="")
    {

        $config_livechat = Config_livechat::where('channel', 'site')
                                            // ->where('livechat_provider', '!=', '1')
                                            ->whereNotNull('livechat_provider')
                                            ->select('livechat_key', 'livechat_provider', 'livechat_script', 'livechat_id')
                                            // ->toSql();
                                            ->first();
                                            // dd($config_livechat);

        if(!empty($config_livechat)){
            //1:Hkb,2:Zopim,3:Livechat,4:Tawkto,5:Jivochat
            // create empty property
            /*
            *   blade detect if livechat_setting is empty, not show any live chat
            *   livechat_setting used for both desktop js script and mobile direct link URL
            *   livechat_js is only for mobile if direct link are not available but can open with simple javascript (check provider # 5 as example)
            */
            $config_livechat->livechat_setting = '';
            switch ($config_livechat->livechat_provider) {
                case 1:
                    if(!$global){
                        // break;
                    }
                    $user = 'guest';
                    $additional_param = "";
                    if(auth()->check()){
                        $config_bank = Config_bank::where('bank_id', auth()->user()->user_bank)->first(['bank_id','status','bank_name']);
                        if(!empty($config_bank)){
                            $user = auth()->user()->user_name;
                            $additional_param="&bkname=".strtolower($config_bank->bank_name)."&bkstatus=".($config_bank->status) ?? 0;
                        }
                    }
                    /*$livechat['user']
                    $livechat['key']
                    $livechat['prov']
                    $livechat['hash']*/
                    $config_livechat->user = $user;
                    $key = $config_livechat->livechat_key;
                    $config_livechat->prov = $config_livechat->livechat_id;
                    $config_livechat->hash = md5($user.$key.$config_livechat->prov);
                    $config_livechat->livechat_setting = "
                    (function() { 
                    var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                    lc.src = '".config('global.web_livechat_url')."?data=".$config_livechat->user."&prov=".$config_livechat->prov."&key=".$config_livechat->hash."&channel=".get_channel_id().$additional_param."'; 
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s); 
                    })();";
                    if(config('global.channel') == 'm'){
                        $config_livechat->livechat_setting = 'javascript:popupLivechat("'.config('global.m_livechat_url').'?user='.$config_livechat->user.'&prov='.$config_livechat->prov.'&key='.$config_livechat->hash.$additional_param.'")';
                    }
                    break;
                case 2:
                    $config_livechat->livechat_setting = 'window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
                        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
                        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
                        $.src="https://v2.zopim.com/?'.$config_livechat->livechat_key.'";z.t=+new Date;$.
                        type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");';
                    if(config('global.channel') == 'm'){
                        $config_livechat->livechat_setting = 'javaScript:popupLivechat("https://v2.zopim.com/widget/popout.html?key='.$config_livechat->livechat_key.'");';
                    }
                    break;
                case 3:
                    $config_livechat->livechat_setting = "window.__lc = window.__lc || {}; 
                        window.__lc.license = ".$config_livechat->livechat_key."; 
                        (function() { 
                        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true; 
                        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js'; 
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s); 
                        })(); ";
                    if(config('global.channel') == 'm'){
                        $mobile_url = 'https://lc.chat/now/'.$config_livechat->livechat_key;
                            if($config_livechat->livechat_script){
                            $my_arr = explode('mobile_url', $config_livechat->livechat_script);
                            if (isset($my_arr[1])){
                                $semi_colon = stripos($my_arr[1], ';') - 1;
                                $myvar_text = substr($my_arr[1], stripos($my_arr[1], '=') + 1);
                                $new_url = explode(';', $myvar_text)[0];
                                $new_url = trim(preg_replace('/[\'"]+/', '', $new_url));
                                if($new_url){
                                    $mobile_url = $new_url;
                                }
                            }
                        }
                        $config_livechat->livechat_setting = 'javaScript:popupLivechat("'.$mobile_url.'");';
                    }
                    break;
                case 4:
                    $lc = $config_livechat->livechat_script;//var tawk_default='1f1q8n9oi';
                    $first = strlen("tawk_default=") + stripos($lc, "tawk_default=") + 1;
                    $get_first = substr($lc, $first);
                    $get_id = substr($get_first, 0, strpos($get_first, ";") - 1);
                    if(!$get_id){
                        $get_id = 'default';
                    }
                    $config_livechat->livechat_setting = "var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                        (function(){
                        var s1=document.createElement('script'),s0=document.getElementsByTagName('script')[0];
                        s1.async=true;
                        s1.src='https://embed.tawk.to/".$config_livechat->livechat_key."/".$get_id."';
                        s1.charset='UTF-8';
                        s1.setAttribute('crossorigin','*');
                        s0.parentNode.insertBefore(s1,s0);
                        })();";
                    if(config('global.channel') == 'm'){
                        $config_livechat->livechat_setting = 'javaScript:popupLivechat("https://tawk.to/chat/'.$config_livechat->livechat_key.'/'.$get_id.'");';
                    }
                    break;
                case 5:
                    $config_livechat->livechat_setting = "(function() { 
                    var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true; 
                    lc.setAttribute('id','ze-snippet');
                    lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.zdassets.com/ekr/snippet.js?key=".$config_livechat->livechat_key."'; 
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s); 
                    })();";
                    

                    if(config('global.channel') == 'm'){
                        $config_livechat->livechat_js = $config_livechat->livechat_setting.'hideHTMLElement(".zEWidget-launcher",2000)' ; 
                        $config_livechat->livechat_setting = 'javascript:zE.activate();';
                    }
                    break;
                case 6:
                    $config_livechat->livechat_setting = "(function() {
                    var occ = document.createElement('script'); occ.type = 'text/javascript'; occ.async = true;
                    occ.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'www.onlinechatcenters.com/code-".$config_livechat->livechat_key.".js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(occ, s);
                    })();";
                    if(config('global.channel') == 'm'){
                        $config_livechat->livechat_setting = 'javaScript:popupLivechat("https://www.onlinechatcenters.com/chat/'.$config_livechat->livechat_key.'");';
                    }
                    break;
                case 7:
                    $config_livechat->livechat_setting = "
                    var lhnAccountN = '".$config_livechat->livechat_key."-1';
                    ".$config_livechat->livechat_script."
                    (function() { 
                    var lc = document.createElement('script'); lc.type = 'text/javascript';
                    lc.setAttribute('id','lhnscript');
                    lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'www.livehelpnow.net/lhn/widgets/chatbutton/lhnchatbutton-current.min.js'; 
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s); 
                    })();";

                    //function to get lhnWindowN value in livechat script
                    $arr = explode('lhnWindowN',$config_livechat->livechat_script);
                    $zzwindow = '';
                    if(count($arr) > 1){
                        $arr2 = explode(';', $arr[1])[0];
                        $hasil = trim(str_replace('=','',$arr2));
                        $zzwindow = '&zzwindow='.$hasil;
                    }
                    //http://www.livehelpnow.net/lhn/lcv.aspx?lhnid=36741&zzwindow=45715
                    if(config('global.channel') == 'm'){
                        $config_livechat->livechat_setting = 'javaScript:popupLivechat("https://www.livehelpnow.net/lhn/lcv.aspx?lhnid='.$config_livechat->livechat_key.$zzwindow.'");';
                    }
                    break;
                case 8:
                    $config_livechat->livechat_setting = "(function() {
                    var lc_ct = document.createElement('script'); lc_ct.type = 'text/javascript'; lc_ct.async = true;
                    lc_ct.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'code.tidio.co/".$config_livechat->livechat_key.".js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc_ct, s);
                    })();";
                    if(config('global.channel') == 'm'){
                        $config_livechat->livechat_setting = 'javaScript:popupLivechat("https://www.tidiochat.com/chat/'.$config_livechat->livechat_key.'");';
                    }
                    break;
                case 9:
                    $config_livechat->livechat_setting = 'var se0odK=document.createElement("script");se0odK.type="text/javascript";var se0odKs="https://image.providesupport.com/js/'.$config_livechat->livechat_key.'/safe-standard.js?ps_h=0odK&ps_t="+new Date().getTime();se0odK.src=se0odKs;var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(se0odK, s);';
                    if(config('global.channel') == 'm'){
                        $config_livechat->livechat_setting = 'javaScript:popupLivechat("https://messenger.providesupport.com/messenger/'.$config_livechat->livechat_key.'.html");';
                    }
                    break;
                case 10:
                    /*t.site_id = 233965, 
                    t.main_code_plan = 470,
                    t.chat_buttons.push({
                        code_plan: 470,
                        div_id: 'comm100-button-470'
                    }), */
                    /*$config_livechat->livechat_setting = "var Comm100API = Comm100API || {};
                    (function(t) {
                        function e(e) {
                            var a = document.createElement(\"script\"),
                            c = document.getElementsByTagName(\"script\")[0];
                            a.type = \"text/javascript\", a.async = !0, a.src = e + t.site_id, c.parentNode.insertBefore(a, c)
                        }
                        t.chat_buttons = t.chat_buttons || [], 
                        ".$config_livechat->livechat_script."
                        e(\"https://vue.comm100.com/livechat.ashx?siteId=\"), 
                        setTimeout(function() {
                            t.loaded || e(\"https://vue1.comm100.com/livechat.ashx?siteId=\")
                        }, 5e3)
                    })(Comm100API || {});
                    var newDiv = document.createElement('div');
                    newDiv.id = Comm100API.chat_buttons[0].div_id;
                    document.body.appendChild(newDiv);";
                    if(config('global.channel') == 'm'){
                        $config_livechat->livechat_js = "
                        t.chat_buttons = t.chat_buttons || [];
                        ".$config_livechat->livechat_script;
                        $config_livechat->livechat_setting = 'javaScript:popupLivechat("https://vue.comm100.com/chatWindow.aspx?siteId='.$config_livechat->livechat_key.'&planId="+t.chat_buttons.code_plan);';
                    }*/

                    // old comm100
                    $config_livechat->livechat_setting = "
                    var Comm100API = Comm100API || new Object;
                    Comm100API.chat_buttons = Comm100API.chat_buttons || [];
                    var comm100_chatButton = new Object;
                    ".$config_livechat->livechat_script."
                    var comm100_lc = document.createElement('script');
                    comm100_lc.type = 'text/javascript';
                    comm100_lc.async = true;
                    if(typeof comm100_lc_src !== 'undefined'){
                        comm100_lc.src = comm100_lc_src + Comm100API.site_id;
                    }else{
                        comm100_lc.src = 'https://vue.livelyhelp.chat/livechat.ashx?siteId=' + Comm100API.site_id;
                    }
                    var comm100_s = document.getElementsByTagName('script')[0];
                    var comm100_b = document.getElementsByTagName('body')[0];
                    comm100_s.parentNode.insertBefore(comm100_lc, comm100_s);
                    var comm100_cbtn = document.createElement('div');
                    comm100_cbtn.id = comm100_chatButton.div_id;
                    comm100_b.append(comm100_cbtn);

                    setTimeout(function() {
                        if (!Comm100API.loaded) {
                            var lc = document.createElement('script');
                            lc.type = 'text/javascript';
                            lc.async = true;
                            lc.src = 'https://vue.comm100.com/livechat.ashx?siteId=' + Comm100API.site_id;
                            var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(lc, s);
                        }
                    }, 5000)";
                    if(config('global.channel') == 'm'){
                        $config_livechat->livechat_js = "
                        var Comm100API = Comm100API || new Object;
                        Comm100API.chat_buttons = Comm100API.chat_buttons || [];
                        var comm100_chatButton = new Object;
                        ".$config_livechat->livechat_script."
                        if(typeof comm100_lc_src_m !== 'undefined'){
                            comm100_lc_src_m = comm100_lc_src_m + Comm100API.site_id;
                        }else{
                            comm100_lc_src_m = 'https://vue.livelyhelp.chat/chatWindow.aspx?siteId=' + Comm100API.site_id;
                        }";

                        $config_livechat->livechat_setting = 'javaScript:popupLivechat(comm100_lc_src_m+"&planId="+comm100_chatButton.code_plan);';
                    }
                    break;
                default:
                    $config_livechat = [];
                    return $config_livechat;
                    break;
            }
        }        
        //dd($config_livechat);
        return $config_livechat;
    }

    public function loggedToFile($message=null,$type=null){

        $message['function'] = debug_backtrace()[1]['function'] ?? "";
        $message['class'] = debug_backtrace()[1]['class'] ?? "";

        $message = json_encode($message);
        switch ($type) {
            case 'emergency':
                Log::info($message);
                break;
            case 'alert':
                Log::alert($message);
                break;
            case 'critical':
                Log::critical($message);
                break;
            case 'error':
                Log::error($message);
                break;
            case 'warning':
                Log::warning($message);
                break;
            case 'notice':
                Log::notice($message);
                break;
            case 'debug':
                Log::debug($message);
                break;
            default:
                Log::info($message);
                break;
        }
        return true;
    }

    public static function get_active_db($data=[]){
        $data['partners'] = Partner_web::orderBy('name')->get(['web_id', 'name','db_name']);
        $listDB = $data['partners']->unique('db_name')->pluck('db_name')->toArray();
        array_unshift($listDB, config('database.default'));
        $data['database'] = array_unique($listDB); 
        return $data['database'];
    }
} 