<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {


    Route::post('balance_update/{user_name}', 'ApiController@updateBalance')->name("balance.update");

    Route::get('profile/balance/{code}', 'ProfileController@balance')->name("profile.balance");

    Route::middleware('throttle:1,0.05')->group(
        function () {
            Route::post('initiateTransfer', 'ApiController@initiateTransfer')->name('transfer.initiate');
        }
    );
});
